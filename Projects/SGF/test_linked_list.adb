with ada.integer_text_io, ada.text_io;
use ada.integer_text_io, ada.text_io;

with linked_list;

procedure test_linked_list is

    -- Instantiate generic linked list
    package linked_list_int is new linked_list (integer, integer'image, "=");
    use linked_list_int;

    procedure print_test_result (nb_tests_ok : in integer;
                                 nb_tests_error : in integer) is
    begin
        put(nb_tests_ok, 0);put("/");put(nb_tests_error+nb_tests_ok, 0);put(" tests ok");
        if nb_tests_error > 0 then
            put(", ");put(nb_tests_error, 0);put(" errors");
        end if;
        new_line;
    end print_test_result;

    procedure test_is_empty is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        list : linked_list_int.p_node;
    begin
        put_line("test is empty");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        init_list(list);

        -- Empty
        if is_empty(list) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: init list not empty");
        end if;

        -- Not empty
        insert_first(list, 1);
        if not is_empty(list) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: list with element empty");
        end if;


        print_test_result(nb_tests_ok, nb_tests_error);
    end test_is_empty;

    procedure test_has_next is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        list : linked_list_int.p_node;
    begin
        put_line("test has next");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        init_list(list);
        insert_last(list, 1);

        -- Has not next
        if not has_next(list) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: init list has next");
        end if;

        -- Has next
        insert_last(list, 2);
        if has_next(list) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: list with next has not next");
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_has_next;

    procedure test_next is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        list : linked_list_int.p_node;
        result : integer;
        expected_result : integer;
    begin
        put_line("test next");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        init_list(list);
        insert_last(list, 1);
        insert_last(list, 2);
        insert_last(list, 3);
        insert_last(list, 4);

        next(list);
        -- Next
        result := get_value(list);
        expected_result := 2;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: next not working");
            put("Got: ");put(result, 0);new_line;
            put("Expected: ");put(expected_result, 0);new_line;
        end if;

        next(list);
        -- Next
        result := get_value(list);
        expected_result := 3;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: next not working");
            put("Got: ");put(result, 0);new_line;
            put("Expected: ");put(expected_result, 0);new_line;
        end if;

        next(list);
        -- Next
        result := get_value(list);
        expected_result := 4;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: next not working");
            put("Got: ");put(result, 0);new_line;
            put("Expected: ");put(expected_result, 0);new_line;
        end if;

        next(list);
        -- Next
        if is_empty(list) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: next not working");
            put("Got: ");put(get_value(list), 0);new_line;
            put("Expected: empty list");new_line;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_next;

    procedure test_get_value is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        list : linked_list_int.p_node;
        result : integer;
        expected_result : integer;
    begin
        put_line("test get value");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        init_list(list);
        insert_last(list, 1);

        -- Get value
        result := get_value(list);
        expected_result := 1;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: get wrong value");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        init_list(list);
        insert_last(list, 45);

        -- Get value
        result := get_value(list);
        expected_result := 45;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: get wrong value");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_get_value;

    procedure test_set_value is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        list : linked_list_int.p_node;
        result : integer;
        expected_result : integer;
    begin
        put_line("test set value");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        init_list(list);
        insert_last(list, 12);
        insert_last(list, 13);

        -- Set value
        set_value(list, 87);
        result := get_value(list);
        expected_result := 87;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: set value not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        -- Unchanged list
        next(list);
        result := get_value(list);
        expected_result := 13;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: other elements of the list are modified with set value");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;


        print_test_result(nb_tests_ok, nb_tests_error);
    end test_set_value;

    procedure test_insert_last is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        list : linked_list_int.p_node;
        result : integer;
        expected_result : integer;
    begin
        put_line("test insert last");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        init_list(list);
        insert_last(list, 1);
        insert_last(list, 2);
        insert_last(list, 3);
        insert_last(list, 4);

        -- Insert 1
        result := get_value(list);
        expected_result := 1;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        -- Insert 2
        next(list);
        result := get_value(list);
        expected_result := 2;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        -- Insert 3
        next(list);
        result := get_value(list);
        expected_result := 3;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        -- Insert 4
        next(list);
        result := get_value(list);
        expected_result := 4;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_insert_last;

    procedure test_insert_first is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        list : linked_list_int.p_node;
        result : integer;
        expected_result : integer;
    begin
        put_line("test insert first");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        init_list(list);
        insert_first(list, 1);
        insert_first(list, 2);
        insert_first(list, 3);
        insert_first(list, 4);

        -- List is now:
        -- [4, 3, 2, 1]

        -- Insert 4
        result := get_value(list);
        expected_result := 4;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        -- Insert 3
        next(list);
        result := get_value(list);
        expected_result := 3;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        -- Insert 2
        next(list);
        result := get_value(list);
        expected_result := 2;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        -- Insert 1
        next(list);
        result := get_value(list);
        expected_result := 1;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_insert_first;

    procedure test_insert_after is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        list : linked_list_int.p_node;
        result : integer;
        expected_result : integer;
        
        current : linked_list_int.p_node;
    begin
        put_line("test insert after");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        init_list(list);
        insert_last(list, 1);
        insert_last(list, 2);
        insert_last(list, 3);

        current := list;
        next(current);
        insert_after(list, current, 45);


        -- List is now:
        --        current
        --  list  |
        --  v     vv
        -- [1, 2, 45, 3]

        result := get_value(list);
        expected_result := 1;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(list);
        result := get_value(list);
        expected_result := 2;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(list);
        result := get_value(list);
        expected_result := 45;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(list);
        result := get_value(list);
        expected_result := 3;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_insert_after;

    procedure test_insert_before is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        list : linked_list_int.p_node;
        result : integer;
        expected_result : integer;
        
        current : linked_list_int.p_node;
    begin
        put_line("test insert before");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        init_list(list);
        insert_last(list, 1);
        insert_last(list, 2);
        insert_last(list, 3);

        current := list;
        next(current);
        insert_before(list, current, 45);


        -- List is now:
        --         current
        --  list   |
        --  v      v
        -- [1, 45, 2, 3]

        result := get_value(list);
        expected_result := 1;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(list);
        result := get_value(list);
        expected_result := 45;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(list);
        result := get_value(list);
        expected_result := 2;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(list);
        result := get_value(list);
        expected_result := 3;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: insertion not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_insert_before;

    procedure test_copy is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        list : linked_list_int.p_node;
        result : integer;
        expected_result : integer;
        last_to_copy : linked_list_int.p_node;
        
        copy : linked_list_int.p_node;
    begin
        put_line("test copy");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        init_list(list);
        insert_last(list, 1);
        insert_last(list, 2);
        insert_last(list, 3);
        insert_last(list, 4);

        -- Copy all list
        init_list(last_to_copy);
        linked_list_int.copy(list, copy, last_to_copy);

        -- Copied list is:
        -- [1, 2, 3, 4]

        result := get_value(copy);
        expected_result := 1;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: copy not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(copy);
        result := get_value(copy);
        expected_result := 2;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: copy not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(copy);
        result := get_value(copy);
        expected_result := 3;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: copy not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(copy);
        result := get_value(copy);
        expected_result := 4;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: copy not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        -- Copy part of list
        last_to_copy := get_next(list);
        linked_list_int.copy(list, copy, last_to_copy);

        -- Copied list is:
        -- [1, 2]
        
        result := get_value(copy);
        expected_result := 1;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: copy not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(copy);
        result := get_value(copy);
        expected_result := 2;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: copy not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(copy);
        if is_empty(copy) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: copy not working");
            put("Got: ");put(get_value(copy), 0);
            new_line;
            put("Expected: empty list");
            new_line;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_copy;

    procedure test_remove is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        list : linked_list_int.p_node;
        result : integer;
        expected_result : integer;
        
        current : linked_list_int.p_node;
    begin
        put_line("test remove");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        init_list(list);
        insert_last(list, 1);
        insert_last(list, 2);
        insert_last(list, 45);
        insert_last(list, 3);
        current := list;

        -- Remove element
        remove(list, 45);
        -- List is now:
        -- [1, 2, 3]
        result := get_value(current);
        expected_result := 1;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: remove not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(current);
        result := get_value(current);
        expected_result := 2;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: remove not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(current);
        result := get_value(current);
        expected_result := 3;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: remove not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        -- Remove node
        remove(list, list);
        current := list;
        -- List is now:
        -- [2, 3]

        result := get_value(current);
        expected_result := 2;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: remove not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        next(current);
        result := get_value(current);
        expected_result := 3;
        if result = expected_result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: remove not working");
            put("Got: ");put(result, 0);
            new_line;
            put("Expected: ");put(expected_result, 0);
            new_line;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_remove;

    procedure test_equal is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        list : linked_list_int.p_node;
        
        same : linked_list_int.p_node;
    begin
        put_line("test equal");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        init_list(list);
        insert_last(list, 1);
        insert_last(list, 2);
        insert_last(list, 3);

        init_list(same);
        insert_last(same, 1);
        insert_last(same, 2);
        insert_last(same, 3);

        if equal(list, same) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: lists are not equal");
            put("Got: ");display(list);
            new_line;
            put("Expected: ");display(same);
            new_line;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_equal;


begin

    test_insert_last;
    test_insert_first;
    test_get_value;
    test_set_value;
    test_insert_after;
    test_insert_before;
    test_is_empty;
    test_has_next;
    test_next;
    test_copy;
    test_remove;
    test_equal;

end test_linked_list;