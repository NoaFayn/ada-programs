package body log is

    procedure error (msg : string) is
    begin
        put("[ERROR] ");
        put_line(msg);
    end error;
    procedure error (msg : better_string) is
    begin
        put("[ERROR] ");
        print(msg);
        new_line;
    end error;

    procedure info (msg : string) is
    begin
        put("[INFO] ");
        put_line(msg);
    end info;
    procedure info (msg : better_string) is
    begin
        put("[INFO] ");
        print(msg);
        new_line;
    end info;

    procedure debug (msg : string) is
    begin
        put("[DEBUG] ");
        put_line(msg);
    end debug;
    procedure debug (msg : better_string) is
    begin
        put("[DEBUG] ");
        print(msg);
        new_line;
    end debug;

    procedure warning (msg : string) is
    begin
        put("/!\ ");
        put_line(msg);
    end warning;
    procedure warning (msg : better_string) is
    begin
        put("/!\ ");
        print(msg);
        new_line;
    end warning;

end log;