
package body address is

    -- ===================
    -- Internal procedures
    -- ===================
    -- Role: Converts the index in the values of the address to its corresponding unit character.
    -- Here is the convertion table:
    -- | 1 | 2 | 3 | 4 | 5 |
    -- | D | K | M | G | T |
    -- Parameter: index - index to convert
    -- Returns: character corresponding to the index
    -- Pre: 1 <= index <= 5
    -- Post:
    function unit_of_index (index : in integer) return character is
    begin
        case index is
            when 1 => return 'D';
            when 2 => return 'K';
            when 3 => return 'M';
            when 4 => return 'G';
            when 5 => return 'T';
            when others => return '?';
        end case;
    end unit_of_index;

    procedure update_max_unit (a : in out t_address) is
        found_max_unit : boolean;
        tmp : integer;
    begin
        found_max_unit := false;
        for i in reverse 1..5 loop
            tmp := a.values(i);
            if not found_max_unit then
                if tmp /= 0 then
                    found_max_unit := true;
                    a.max_unit := address_unit'val(i-1); -- -1 because starts with 0
                end if;
            end if;
        end loop;
    end update_max_unit;
    

    -- =======
    -- PACKAGE
    -- =======

    procedure init_address (a : out t_address;
                            val : in t_value) is
        
    begin
        a := (values => val, max_unit => DIGIT);
        -- Find max unit
        update_max_unit(a);
    end init_address;

    function add (a1 : in t_address;
                  a2 : in t_address) return t_address is
        tmp : integer;
        result : t_address;
        sustain : integer;
    begin
        result := (values => (others => 0), max_unit => DIGIT);
        sustain := 0;
        for i in 1..5 loop
            tmp := a1.values(i) + a2.values(i) + sustain;
            sustain := 0;
            if tmp >= 1000 then
                -- Check overflow
                if i = 5 then
                    raise address_overflow;
                end if;
                -- Sustain
                sustain := 1;
                tmp := tmp - 1000;
            end if;
            -- Store addition
            result.values(i) := tmp;
            -- Store max unit
            if tmp /= 0 then
                result.max_unit := address_unit'val(i-1); -- -1 because start at 0
            end if;
        end loop;

        return result;
    end add;
    
    function remove (a1 : in t_address;
                     a2 : in t_address) return t_address is
        result : t_address;
        tmp : integer;
        found_max_unit : boolean;

        index : integer;
    begin
        found_max_unit := false;
        result := (values => (others => 0), max_unit => DIGIT);
        for i in reverse 1..5 loop
            tmp := a1.values(i) - a2.values(i);
            -- Address cannot be negative
            if tmp < 0 then
                -- Try to remove from bigger units
                index := i+1;
                while index <= 5 and then result.values(index) = 0 loop
                    -- Find the closest unit that is not 0
                    index := index + 1;
                end loop;
                if index > 5 or else result.values(index) = 0 then
                    -- Not found
                    raise address_negative;
                else
                    -- Found an unit with values
                    -- Remove 1 from this index
                    result.values(index) := result.values(index) - 1;
                    -- All units until last one is set to 999
                    for j in reverse i+1..index-1 loop
                        result.values(j) := 999;
                    end loop;
                    -- Last one is set to 1000 - a2.value
                    result.values(i) := 1000 - a2.values(i);
                end if;
            else
                result.values(i) := tmp;
            end if;
        end loop;
        -- Update max unit
        update_max_unit(result);

        return result;
    end remove;
    
    function equal (a1 : in t_address;
                    a2 : in t_address) return boolean is
    begin
        for i in reverse 1..5 loop
            if a1.values(i) /= a2.values(i) then
                return false;
            end if;
        end loop;
        return true;
    end equal;

    function to_bs (addr : in t_address) return better_string is
        s : better_string;
        address_int_length : integer;
        max_index : integer;
    begin
        s := string_to_better("", 0);
        -- Get only the two biggest units of the address
        max_index := address_unit'pos(addr.max_unit)+1;
        for i in reverse max_index-1..max_index loop
            if i > 0 then
                if addr.values(i) > 0 then
                    -- Value of address
                    address_int_length := integer'image(addr.values(i))'length;
                    s.str(s.size+1..s.size+address_int_length) := integer'image(addr.values(i));
                    s.size := s.size + address_int_length;
                    -- Unit of address
                    s.str(s.size+1) := unit_of_index(i);
                    s.size := s.size + 1;
                end if;
            end if;
        end loop;
        return s;
    end to_bs;

    function is_zero (addr : in t_address) return boolean is
    begin
        if addr.max_unit /= DIGIT then
            return false;
        end if;
        if addr.values(1) /= 0 then
            return false;
        end if;
        return true;
    end is_zero;

    function less (a1 : in t_address;
                   a2 : in t_address) return boolean is
        ignored : t_address;
    begin
        ignored := remove(a1, a2);
        return false;
    exception
        when address_negative => return true;
    end less;

    function greater (a1 : in t_address;
                      a2 : in t_address) return boolean is
        ignored : t_address;
    begin
        ignored := remove(a2, a1);
        return false;
    exception
        when address_negative => return true;
    end greater;

    function image (addr : in t_address) return better_string is
        result : better_string;
        int_length : integer;
    begin
        result := tools.string_to_better("", 0);
        for i in reverse 1..5 loop
            int_length := addr.values(i)'image'length;
            result.str(result.size+1..result.size+int_length) := addr.values(i)'image;
            result.size := result.size + int_length;
            if i /= 1 then
                result.str(result.size+1) := ' ';
                result.size := result.size + 1;
            end if;
        end loop;
        return result;
    end image;

    procedure display (addr : in t_address) is
    begin
        for i in reverse 1..5 loop
            put(addr.values(i), 0);
            put(unit_of_index(i));
            if i /= 1 then
                put(" ");
            end if;
        end loop;
    end display;
end address;