with sgf;
use sgf;
with tools;
use tools;
with log;
use log;
with ada.text_io;
use ada.text_io;
with ada.integer_text_io;
use ada.integer_text_io;
with address; use address;
with disk; use disk;
with address; use address;

procedure main is
    -- CONSTANTS
    INTERFACE_MENU      : constant integer := 1;
    INTERFACE_CLI       : constant integer := 2;

    PATH_CURRENT_DIR    : constant better_string := (str => "." & (2..MAX_STRING_LENGTH => character'val(0)), size => 1);
    PATH_PARENT_DIR     : constant better_string := (str => ".." & (3..MAX_STRING_LENGTH => character'val(0)), size => 2);


    procedure hardcoded_init (fs : out t_sgf) is
    begin
        info("Formatting file system...");
        initialise_sgf(fs);
        info("done!");

        info("Generating default folders...");
        info("home...");
        create_dir(fs, string_to_better("home", 4));
        create_dir(fs, string_to_better("home/lambda", 11));
        create_dir(fs, string_to_better("home/lambda/Documents", 21));
        info("bin...");
        create_dir(fs, string_to_better("bin", 3));
        info("root...");
        create_dir(fs, string_to_better("root", 4));
        info("opt...");
        create_dir(fs, string_to_better("opt", 3));
        info("done!");

        info("Generating test files...");
        create_file(fs, string_to_better("home/lambda/Documents/test.txt", 30));
        create_file(fs, string_to_better("root/firewall", 13));
        info("done!");
    end hardcoded_init;

    -- Role: Handles the command.
    -- Parameters: fs - file system
    -- command - command to handle
    -- Pre:
    -- Post:
    procedure command_handler (fs : in out t_sgf;
                               c : in better_string) is
        cmd : tools.linked_list_string.p_node;
        command : better_string;
        arg : better_string;
        file_name : better_string;
        path : better_string;
        size : integer;
        new_size : t_address;
        path_dest : better_string;
    begin
        -- Parse user command
        cmd := split(c, ' ');
        
        -- Handle command
        command := tools.linked_list_string.get_value(cmd);
                
        -- Acquire the current working directory
        -- pwd
        if command.size = 3 and command.str(1..3) = "pwd" then
            print(get_current_dir_path(fs)); new_line;
        
        -- Creation of a file
        -- touch <file_name>
        elsif command.size = 5 and command.str(1..5) = "touch" then
            -- Get argument of command
            if tools.linked_list_string.has_next(cmd) then
                tools.linked_list_string.next(cmd);
                file_name := tools.linked_list_string.get_value(cmd);
                
                if exists(fs, file_name) then
                -- Update file if exists
                    update_file_timestamp(fs, file_name);
                else
                -- Create file if not exists
                    create_file(fs, file_name);
                end if;
            else
                error("Usage: touch <file_name>");
            end if;
        
        -- Display the content of the path
        -- ls [-r] [path]
        elsif command.size = 2 and command.str(1..2) = "ls" then
            if tools.linked_list_string.has_next(cmd) then
                tools.linked_list_string.next(cmd);
                arg := tools.linked_list_string.get_value(cmd);
                if arg.size = 2 and arg.str(1..2) = "-r" then
                    -- Recursive display
                    if tools.linked_list_string.has_next(cmd) then
                        tools.linked_list_string.next(cmd);
                        path := tools.linked_list_string.get_value(cmd);
                        display_content_recursive(fs, path);
                    else
                        -- Display the content of the current directory recursively
                        display_content_recursive(fs, PATH_CURRENT_DIR);
                    end if;
                else
                    path := tools.linked_list_string.get_value(cmd);
                    display_content(fs, path);
                end if;
            else
                -- Current path
                display_content(fs, PATH_CURRENT_DIR);
            end if;

        -- Creation of a directory
        -- mkdir <path>
        elsif command.size = 5 and command.str(1..5) = "mkdir" then
            if tools.linked_list_string.has_next(cmd) then
                tools.linked_list_string.next(cmd);
                file_name := tools.linked_list_string.get_value(cmd);
                create_dir(fs, file_name);
            else
                error("Usage: mkdir <dir_name>");
            end if;

        -- Change the current directory
        -- cd <path>
        elsif command.size = 2 and command.str(1..2) = "cd" then
            if tools.linked_list_string.has_next(cmd) then
                tools.linked_list_string.next(cmd);
                path := tools.linked_list_string.get_value(cmd);
                set_current_dir(fs, path);
            else
                error("Usage: cd <path>");
            end if;

        -- Remove a file or directory
        -- rm [-r] <path>
        elsif command.size = 2 and command.str(1..2) = "rm" then
            if tools.linked_list_string.has_next(cmd) then
                tools.linked_list_string.next(cmd);
                arg := tools.linked_list_string.get_value(cmd);
                if arg.size = 2 and arg.str(1..2) = "-r" then
                    if tools.linked_list_string.has_next(cmd) then
                        tools.linked_list_string.next(cmd);
                        path := tools.linked_list_string.get_value(cmd);
                        -- Accept to remove directory
                        remove_file(fs, path, false);
                    else
                        error("Usage: rm -r <path>");
                    end if;
                else
                    path := tools.linked_list_string.get_value(cmd);
                    -- Only remove file
                    begin
                        remove_file(fs, path, true);
                    exception
                        when is_a_directory => warning("Use ""rm -r"" to remove directories");
                    end;
                end if;
            else
                error("Usage: rm <file_name>");
            end if;

        -- Change the size of a file
        -- write <path> <newSize>
        elsif command.size = 5 and command.str(1..5) = "write" then
            if tools.linked_list_string.has_next(cmd) then
                tools.linked_list_string.next(cmd);
                path := tools.linked_list_string.get_value(cmd);
                if tools.linked_list_string.has_next(cmd) then
                    tools.linked_list_string.next(cmd);
                    size := integer'value(tools.value(tools.linked_list_string.get_value(cmd)));
                    -- Check size > 0
                    if size <= 0 then
                        error("Size must be positive");
                    else
                        -- TODO: Parser for the size
                        warning("TODO: Parser for the size. Hardcoded: ?G");
                        address.init_address(new_size, (4 => size, others => 0));
                        change_file_size(fs, path, new_size);
                    end if;
                else
                    error("Usage: write <path> <newSize>");
                end if;
            else
                error("Usage: write <path> <newSize>");
            end if;

        -- Format file system
        -- format
        elsif command.size = 6 and command.str(1..6) = "format" then
            if tools.linked_list_string.has_next(cmd) then
                error("Usage: format");
            else
                initialise_sgf(fs);
            end if;
        
        -- Reboot
        -- reboot
        elsif command.size = 6 and command.str(1..6) = "reboot" then
            if tools.linked_list_string.has_next(cmd) then
                error("Usage: reboot");
            else
                hardcoded_init(fs);
            end if;
        
        -- Move file
        -- mv <path_src> <path_dest>
        elsif command.size = 2 and command.str(1..2) = "mv" then
            if tools.linked_list_string.has_next(cmd) then
                tools.linked_list_string.next(cmd);
                path := tools.linked_list_string.get_value(cmd);
                if tools.linked_list_string.has_next(cmd) then
                    tools.linked_list_string.next(cmd);
                    path_dest := tools.linked_list_string.get_value(cmd);
                    -- Move
                    move_file(fs, path, path_dest);
                else
                    error("Usage: mv <path_src> <path_dest>");
                end if;
            else
                error("Usage: mv <path_src> <path_dest>");
            end if;
        
        -- Copy file
        -- cp <path_src> <path_dest>
        elsif command.size = 2 and command.str(1..2) = "cp" then
            if tools.linked_list_string.has_next(cmd) then
                tools.linked_list_string.next(cmd);
                path := tools.linked_list_string.get_value(cmd);
                if tools.linked_list_string.has_next(cmd) then
                    tools.linked_list_string.next(cmd);
                    path_dest := tools.linked_list_string.get_value(cmd);
                    -- Copy
                    copy_file(fs, path, path_dest);
                else
                    error("Usage: cp <path_src> <path_dest>");
                end if;
            else
                error("Usage: cp <path_src> <path_dest>");
            end if;

        -- List of all possible commands
        -- help
        elsif command.size = 4 and command.str(1..4) = "help" then
            if tools.linked_list_string.has_next(cmd) then
                error("Usage: help");
            else
                put_line("pwd");
                put_line("touch <file_name>");
                put_line("ls [-r] [path]");
                put_line("mkdir <path>");
                put_line("cd <path>");
                put_line("rm [-r] <path>");
                put_line("write <path> <newSize>");
                put_line("format");
                put_line("reboot");
                put_line("mv <path_src> <path_dest>");
                put_line("cp <path_src> <path_dest>");
                put_line("tar <path_src> <path_dest>");
                put_line("shutdown | quit | exit");
            end if;

        -- Archive a directory
        -- tar <path_src> <path_dest>
        elsif command.size = 3 and command.str(1..3) = "tar" then
            if tools.linked_list_string.has_next(cmd) then
                tools.linked_list_string.next(cmd);
                path := tools.linked_list_string.get_value(cmd);
                if tools.linked_list_string.has_next(cmd) then
                    tools.linked_list_string.next(cmd);
                    path_dest := tools.linked_list_string.get_value(cmd);
                    -- Archive
                    archive(fs, path, path_dest);
                else
                    error("Usage: tar <path_src> <path_dest>");
                end if;
            else
                error("Usage: tar <path_src> <path_dest>");
            end if;


        -- Unknown
        else
            error("Unknown command");
        end if;

    exception
        when no_such_file => warning("No such file or directory");
        when not_a_directory => warning("Invalid path");
        when file_already_exists => warning("File already exists");
        when cannot_edit_root => warning("Cannot edit root");
        when cannot_delete_root => warning("Cannot delete root");
        when address_overflow => warning("Unhandled error: Address overflow");
        when address_negative => warning("Address or size cannot be negative");
        when disk_full => warning("Disk memory is full");
        when disk_overflow => warning("Disk capacity cannot be exceeded");
        when disk_wrong_size => warning("Allocation can only be striclty positive");
        when others => warning("Oopsy... Somthing unexpected happened here...");
    end command_handler;


    -- Role: Asks the user which interface he wants to use.
    -- Parameter:
    -- Returns: 1 for the menu interface
    --          2 for the command line interface
    -- Pre:
    -- Post:
    function ask_user_choose_menu return integer is
        answer : integer;
    begin
        put_line("=======================");
        put_line("Which interface to use?");
        put_line("=======================");
        put_line("      (1) Menu         ");
        put_line("   (2) Command line    ");
        put_line("=======================");

        answer := 0;
        while answer /= 1 and answer /= 2 loop
            put("==> ");
            begin
                answer := integer'value(get_line);
            exception
                when constraint_error =>
                    error("invalid input");
                    answer := 0;
            end;
        end loop;
        return answer;
    end ask_user_choose_menu;

    -- Role: Displays the menu of available options to the user.
    -- Parameter:
    -- Pre:
    -- Post:
    procedure display_menu is
    begin
        new_line;
        put_line("Choose an option");
        put_line("----------------");
        put_line("[0] shutdown     (Quit)");
        put_line("[1] pwd          (Print Working Directory)");
        put_line("[2] touch        (Create file)");
        put_line("----------------");
    end display_menu;

    -- Role: Main loop for the menu interface.
    -- Parameter:
    -- Pre:
    -- Post:
    procedure handle_menu_interface is
        quit : boolean;
        option : integer;
    begin
        -- Main loop
        quit := false;
        while not quit loop
            display_menu;
            -- Prompt option to user
            option := 0;
            begin
                option := integer'value(get_line);
            exception
                when constraint_error =>
                    error("invalid option");
                    option := 0;
            end;

            -- Handle option
            case option is
                -- shutdown
                when 0 =>
                    info("shutdown");
                    quit := true;
                -- pwd
                when 1 =>
                    info("pwd");
                -- touch
                when 2 =>
                    info("touch");
                -- unknown
                when others =>
                    error("unknown option");
            end case;
        end loop;
    end handle_menu_interface;

    -- Role: Main loop for the command line interface
    -- Parameter:
    -- Pre:
    -- Post:
    procedure handle_command_line_interface(fs : in out t_sgf) is
        command : better_string;
        quit : boolean;
    begin
        -- Main loop
        quit := false;
        while not quit loop
            -- Get user command
            put("> ");
            get_line(command.str, command.size);
            
            -- Limit the size of the command so it doesn't exceed the max size of the better_string type
            if command.size > MAX_STRING_LENGTH then
                command.size := MAX_STRING_LENGTH;
            else
                null;
            end if;

            -- Quit
            if (command.size = 8 and command.str(1..8) = "shutdown") or (command.size = 4 and command.str(1..4) = "quit") or (command.size = 4 and command.str(1..4) = "exit") then
                quit := true;
                info("shutdown");
            else
                -- Handle command
                command_handler(fs, command);
            end if;
        end loop;
    end handle_command_line_interface;


    -- VARIABLES
    fs          : t_sgf;    -- file system manager
    user_answer : integer;  -- user answer
begin
    hardcoded_init(fs);

    user_answer := ask_user_choose_menu;
    -- Use menu interface
    if user_answer = INTERFACE_MENU then
        handle_menu_interface;
    -- Use command line interface
    elsif user_answer = INTERFACE_CLI then
        handle_command_line_interface(fs);
    -- Unknown interface
    else
        error("Unknown interface");
    end if;
end main;