with ada.text_io; use ada.text_io;
with ada.integer_text_io; use ada.integer_text_io;
with tools; use tools;

package address is

    type t_value is array (1..5) of integer;
    type t_address is private;

    address_overflow    : exception;
    address_negative    : exception;

    -- Digit: base
    -- Kilo: 1000 * digit
    -- Mega: 1000 * kilo
    -- Giga: 1000 * mega
    -- Tera: 1000 * giga
    type address_unit is (DIGIT, KILO, MEGA, GIGA, TERA);
    
    -- Role: Initialises an address
    -- Parameters: a - address to initialise
    -- val : value of the address
    -- unit : unit the value is in
    -- Pre:
    -- Post: a is an address of value val and unit unit
    procedure init_address (a : out t_address;
                            val : in t_value);

    -- Role: Adds two addresses
    -- Parameters: a1 - first address
    -- a2 : second address
    -- Returns: addition of the two addresses. The result is < 999TERA.
    -- Pre:
    -- Post: 
    -- Exception: address_overflow - if the expected result is greater than 999TERA, then the overflow exception is raised
    function add (a1 : in t_address;
                  a2 : in t_address) return t_address;
    
    -- Role: Remove a2 to a1
    -- Parameters: a1 - first address to remove from
    -- a2 : value to remove from a1
    -- Returns: substraction of the two addresses. The result cannot be negative.
    -- Pre:
    -- Post: 
    -- Exception: address_negative - if the result is negative
    function remove (a1 : in t_address;
                     a2 : in t_address) return t_address;
    
    -- Role: Checks if two addresses are equal
    -- Parameters: a1 - first address
    -- a2 : second address
    -- Returns: TRUE if the addresses have the same value and same unit and FALSE otherwise
    -- Pre:
    -- Post:
    function equal (a1 : in t_address;
                    a2 : in t_address) return boolean;
    
    -- Role: Converts this address to a readable string. The string will be of the form:
    -- 1,23T or 5G or 999,999M or 1K or 234
    -- Parameter: addr - address to represent as string
    -- Returns: the representation of the address
    -- Pre:
    -- Post:
    function to_bs (addr : in t_address) return better_string;

    -- Role: Checks if the address value is zero.
    -- Parameter: addr - the address to check
    -- Returns: TRUE if the address is zero and FALSE otherwise
    -- Pre:
    -- Post:
    function is_zero (addr : in t_address) return boolean;

    -- Role: Checks if the first address is less than the second. (a1 < a2)
    -- Parameters: a1 - first address to compare
    -- a2 - second address to compare
    -- Returns: TRUE if a1 < a2 and FALSE otherwise
    -- Pre:
    -- Post:
    function less (a1 : in t_address;
                   a2 : in t_address) return boolean;

    -- Role: Checks if the first address is greater than the second. (a1 > a2)
    -- Parameters: a1 - first address to compare
    -- a2 - second address to compare
    -- Returns: TRUE if a1 > a2 and FALSE otherwise
    -- Pre:
    -- Post:
    function greater (a1 : in t_address;
                      a2 : in t_address) return boolean;

    -- Role: Representation of the address as followed:
    -- ?T ?G ?M ?T ?
    -- Parameter: addr - address to get the representation of
    -- Returns: string representing the address
    -- Pre:
    -- Post:
    function image (addr : in t_address) return better_string;
    
    -- DEBUG
    -- Role: Displays on the standard output the representation of the address.
    -- This procedure is provided for debbugging purpose only. To get a usable representation of the address, one can use the to_bs function.
    -- Parameter: addr - address to display
    -- Pre:
    -- Post:
    procedure display (addr : in t_address);


    private
        -- The address represents a location in the memory
        type t_address is record
            max_unit : address_unit;
            -- Values:
            -- | 1 | 2 | 3 | 4 | 5 |
            -- | D | K | M | G | T |
            values : t_value;
        end record;
end address;