with ada.text_io; use ada.text_io;
with ada.integer_text_io; use ada.integer_text_io;
with tools; use tools;

procedure test_tools is

    procedure print_test_result (nb_tests_ok : in integer;
                                 nb_tests_error : in integer) is
    begin
        put(nb_tests_ok, 0);put("/");put(nb_tests_error+nb_tests_ok, 0);put(" tests ok");
        if nb_tests_error > 0 then
            put(", ");put(nb_tests_error);put(" errors");
        end if;
        new_line;
    end print_test_result;

    procedure test_equal is
        bs_1 : better_string;
        bs_2 : better_string;
        bs_3 : better_string;
        bs_4 : better_string;

        nb_tests_ok : integer;
        nb_tests_error : integer;
    begin
        bs_1 := (str => "azerty" & (7..MAX_STRING_LENGTH => character'val(0)), size => 6);
        bs_2 := (str => "azerty" & (7..MAX_STRING_LENGTH => character'val(0)), size => 6);
        bs_3 := (str => "no_same_size" & (13..MAX_STRING_LENGTH => character'val(0)), size => 12);
        bs_4 := (str => "eqsize" & (7..MAX_STRING_LENGTH => character'val(0)), size => 6);

        put_line("test_equal");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        if bs_1 = bs_2 then
            nb_tests_ok := nb_tests_ok + 1;
        else
            put_line("Error: azerty /= azerty");
            nb_tests_error := nb_tests_error + 1;
        end if;

        if bs_1 = bs_3 then
            put_line("Error: azerty = no_same_size");
            nb_tests_error := nb_tests_error + 1;
        else
            nb_tests_ok := nb_tests_ok + 1;
        end if;

        if bs_1 = bs_4 then
            put_line("Error: azerty = eqsize");
            nb_tests_error := nb_tests_error + 1;
        else
            nb_tests_ok := nb_tests_ok + 1;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_equal;

    procedure test_value is
        bs_1 : better_string;
        bs_2 : better_string;
        bs_3 : better_string;
        bs_4 : better_string;
        
        nb_tests_ok : integer;
        nb_tests_error : integer;
    begin
        bs_1 := (str => "azerty" & (7..MAX_STRING_LENGTH => character'val(0)), size => 6);
        bs_2 := (str => "azerty" & (7..MAX_STRING_LENGTH => character'val(0)), size => 6);
        bs_3 := (str => "no_same_size" & (13..MAX_STRING_LENGTH => character'val(0)), size => 12);
        bs_4 := (str => "eqsize" & (7..MAX_STRING_LENGTH => character'val(0)), size => 6);

        put_line("test_value");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        if value(bs_1) = "azerty" then
            nb_tests_ok := nb_tests_ok + 1;
        else
            put_line("Error: azerty /= azerty");
            nb_tests_error := nb_tests_error + 1;
        end if;
        
        if value(bs_3) = "no_same_size" then
            nb_tests_ok := nb_tests_ok + 1;
        else
            put_line("Error: no_same_size /= no_same_size");
            nb_tests_error := nb_tests_error + 1;
        end if;

        if value(bs_4) = "eqsize" then
            nb_tests_ok := nb_tests_ok + 1;
        else
            put_line("Error: eqsize /= eqsize");
            nb_tests_error := nb_tests_error + 1;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_value;

    procedure test_string_to_better is
        bs_1 : better_string;

        nb_tests_ok : integer;
        nb_tests_error : integer;
    begin
        put_line("test_string_to_better");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        bs_1 := string_to_better("Doctor Strange", 14);
        if value(bs_1) = "Doctor Strange" then
            nb_tests_ok := nb_tests_ok + 1;
        else
            put_line("Error: ""Doctor Strange"" is not fine");
            nb_tests_error := nb_tests_error + 1;
        end if;
        
        bs_1 := string_to_better("", 0);
        if value(bs_1) = "" then
            nb_tests_ok := nb_tests_ok + 1;
        else
            put_line("Error: empty string is not fine");
            nb_tests_error := nb_tests_error + 1;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_string_to_better;

    procedure test_split is
        bs_1 : better_string;
        split_1 : linked_list_string.p_node;

        expected_result : linked_list_string.p_node;
        result : linked_list_string.p_node;

        nb_tests_ok : integer;
        nb_tests_error : integer;

        procedure print_test_error (bs : in better_string;
                                    result : in linked_list_string.p_node;
                                    expected_result : in linked_list_string.p_node) is
        begin
            put_line("Error: split of """ & value(bs) & """ is wrong");
            put("Got: ");
            linked_list_string.display(result);
            put("Expected: ");
            linked_list_string.display(expected_result);
        end print_test_error;
    begin

        put_line("test_split");
        nb_tests_error := 0;
        nb_tests_ok := 0;

        -- Test "/path/to/split"
        bs_1 := (str => "/path/to/split" & (15..MAX_STRING_LENGTH => character'val(0)), size => 14);
        linked_list_string.init_list(expected_result);
        linked_list_string.insert_last(expected_result, tools.string_to_better("", 0));
        linked_list_string.insert_last(expected_result, tools.string_to_better("path", 4));
        linked_list_string.insert_last(expected_result, tools.string_to_better("to", 2));
        linked_list_string.insert_last(expected_result, tools.string_to_better("split", 5));
        result := tools.split(bs_1, '/');
        if linked_list_string.equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(bs_1, result, expected_result);
        end if;

        -- Test "/other/to/split/"
        bs_1 := (str => "/other/to/split/" & (17..MAX_STRING_LENGTH => character'val(0)), size => 16);
        linked_list_string.init_list(expected_result);
        linked_list_string.insert_last(expected_result, tools.string_to_better("", 0));
        linked_list_string.insert_last(expected_result, tools.string_to_better("other", 5));
        linked_list_string.insert_last(expected_result, tools.string_to_better("to", 2));
        linked_list_string.insert_last(expected_result, tools.string_to_better("split", 5));
        result := tools.split(bs_1, '/');
        if linked_list_string.equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(bs_1, result, expected_result);
        end if;

        -- Test "/"
        bs_1 := (str => "/" & (2..MAX_STRING_LENGTH => character'val(0)), size => 1);
        linked_list_string.init_list(expected_result);
        linked_list_string.insert_last(expected_result, tools.string_to_better("", 0));
        result := tools.split(bs_1, '/');
        if linked_list_string.equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(bs_1, result, expected_result);
        end if;

        -- Test "something strange"
        bs_1 := (str => "something strange" & (18..MAX_STRING_LENGTH => character'val(0)), size => 17);
        linked_list_string.init_list(expected_result);
        linked_list_string.insert_last(expected_result, tools.string_to_better("something strange", 17));
        result := tools.split(bs_1, '/');
        if linked_list_string.equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(bs_1, result, expected_result);
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_split;

    procedure test_concat is
        path_1 : linked_list_string.p_node;
        result : better_string;
        expected_result : better_string;

        nb_tests_ok : integer;
        nb_tests_error : integer;

        procedure print_test_error (result : in better_string;
                                    expected_result : in better_string) is
        begin
            put_line("Error: """ & value(expected_result) & """ is not fine");
            put_line("Got: " & value(result));
        end print_test_error;
    begin
        put_line("test_concat");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        -- Test "/normal/path"
        linked_list_string.init_list(path_1);
        linked_list_string.insert_last(path_1, string_to_better("", 0));
        linked_list_string.insert_last(path_1, string_to_better("normal", 6));
        linked_list_string.insert_last(path_1, string_to_better("path", 4));
        expected_result := string_to_better("/normal/path", 12);
        result := tools.concat(path_1, '/');
        if expected_result = result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(result, expected_result);
        end if;

        -- Test "/"
        linked_list_string.init_list(path_1);
        linked_list_string.insert_last(path_1, string_to_better("", 0));
        linked_list_string.insert_last(path_1, string_to_better("", 0));
        expected_result := string_to_better("/", 1);
        result := tools.concat(path_1, '/');
        if expected_result = result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(result, expected_result);
        end if;

        -- Test "../../relative/path"
        linked_list_string.init_list(path_1);
        linked_list_string.insert_last(path_1, string_to_better("..", 2));
        linked_list_string.insert_last(path_1, string_to_better("..", 2));
        linked_list_string.insert_last(path_1, string_to_better("relative", 8));
        linked_list_string.insert_last(path_1, string_to_better("path", 4));
        expected_result := string_to_better("../../relative/path", 19);
        result := tools.concat(path_1, '/');
        if expected_result = result then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(result, expected_result);
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_concat;

    procedure test_basedir is
        bs : better_string;
        dir : better_string;
        expected_dir : better_string;
        filename : better_string;
        expected_filename : better_string;

        nb_tests_ok : integer;
        nb_tests_error : integer;

        procedure print_test_error (bs : in better_string;
                                    dir : in better_string;
                                    filename : in better_string;
                                    expected_dir : in better_string;
                                    expected_filename : in better_string) is
        begin
            put_line("Error: """ & value(bs) & """ not fine");
            put("Got: (" & value(dir) & ")(" & value(filename) & ")");
            put("Expected: (" & value(expected_dir) & ")(" & value(expected_filename) & ")");
        end print_test_error;
    begin
        put_line("test_basedir");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        -- Test "/path/to/file"
        bs := tools.string_to_better("/path/to/file", 13);
        tools.basedir(bs, dir, filename);
        expected_dir := tools.string_to_better("/path/to", 8);
        expected_filename := tools.string_to_better("file", 4);
        if dir = expected_dir and filename = expected_filename then
            nb_tests_ok := nb_tests_ok + 1;
        else
            print_test_error(bs, dir, filename, expected_dir, expected_filename);
        end if;

        -- Test "/path/to/dir/"
        bs := tools.string_to_better("/path/to/dir/", 13);
        tools.basedir(bs, dir, filename);
        expected_dir := tools.string_to_better("/path/to", 8);
        expected_filename := tools.string_to_better("dir", 3);
        if dir = expected_dir and filename = expected_filename then
            nb_tests_ok := nb_tests_ok + 1;
        else
            print_test_error(bs, dir, filename, expected_dir, expected_filename);
        end if;

        -- Test "no path"
        bs := tools.string_to_better("no path", 7);
        tools.basedir(bs, dir, filename);
        expected_dir := tools.string_to_better(".", 1);
        expected_filename := tools.string_to_better("no path", 7);
        if dir = expected_dir and filename = expected_filename then
            nb_tests_ok := nb_tests_ok + 1;
        else
            print_test_error(bs, dir, filename, expected_dir, expected_filename);
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_basedir;


begin

    test_equal;
    test_value;
    test_string_to_better;
    test_split;
    test_concat;
    test_basedir;

end test_tools;