package body linked_list is
    procedure init_list (l : out p_node) is
    begin
        l := null;
    end init_list;

    function is_empty (l : in p_node) return boolean is
    begin
        return l = null;
    end is_empty;

    function has_next (l : in p_node) return boolean is
    begin
        return l.next /= null;
    end has_next;

    procedure next (l : in out p_node) is
    begin
        l := l.next;
    end next;

    function get_value (l : in p_node) return g_element is
    begin
        return l.value;
    end get_value;

    procedure set_value (l : in out p_node;
                        value : in g_element) is
    begin
        l.value := value;
    end set_value;

    procedure insert_last (l : in out p_node;
                           value : in g_element) is
        current : p_node;
    begin
        if l = null then
            l := new t_node'(next => null, value => value);
        else
            current := l;
            while has_next(current) loop
                current := current.next;
            end loop;
            current.next := new t_node'(next => null, value => value);
        end if;
    end insert_last;

    procedure insert_first (l : in out p_node;
                            value : in g_element) is
    begin
        l := new t_node'(next => l, value => value);
    end insert_first;

    procedure insert_after (l : in out p_node;
                            after : in out p_node;
                            value : in g_element) is
        new_node : p_node;
    begin
        if after = null then
            l := new t_node'(next => l, value => value);
        else
            new_node := new t_node'(next => after.next, value => value);
            after.next := new_node;
        end if;
    end insert_after;

    procedure insert_before (l : in out p_node;
                             before : in p_node;
                             value : in g_element) is
        previous : p_node;
        current : p_node;
        new_node : p_node;
        inserted : boolean;
    begin
        if before = null then
            insert_last(l, value);
        else
            if l = before then
                insert_first(l, value);
            else
                previous := l;
                current := l.next;
                inserted := false;
                while not inserted and current /= null loop
                    if current = before then
                        new_node := new t_node'(next => current, value => value);
                        previous.next := new_node;
                        inserted := true;
                    else
                        previous := current;
                        current := current.next;
                    end if;
                end loop;
                -- Edge case if element not inserted => the before is not a node of the list l
                -- This should never happen (based on the pre-condition)
            end if;
        end if;
    end insert_before;

    function get_next (l : in p_node) return p_node is
    begin
        return l.next;
    end get_next;

    procedure copy (list_in : in p_node;
                    list_out : out p_node;
                    last_element : in p_node) is
        in_iterator : p_node;
        out_iterator : p_node;
    begin
        in_iterator := list_in;
        -- First element
        list_out := new t_node'(next => null, value => in_iterator.value);
        out_iterator := list_out;
        -- First element is not last element
        if list_in /= last_element then
            next(in_iterator);
            -- Others
            while in_iterator /= last_element and in_iterator /= null loop
                out_iterator.next := new t_node'(next => null, value => in_iterator.value);
                next(out_iterator);
                next(in_iterator);
            end loop;
            -- Copy the last element if present
            if in_iterator = last_element and in_iterator /= null then
                out_iterator.next := new t_node'(next => null, value => in_iterator.value);
            else
                -- The last element is not in the list or is null
                null;
            end if;
        else
            null;
        end if;

        -- We have the list copied!
        -- And the list_out points to the first element of this list
        -- So we're all good!
    end copy;

    procedure remove (l : in out p_node;
                      elt : in g_element) is
        iterator : p_node;
        over : boolean;
    begin
        if l /= null then
            if l.value = elt then
                l := l.next;
            else
                iterator := l;
                over := false;
                while iterator.next /= null and not over loop
                    if iterator.next.value = elt then
                        iterator.next := iterator.next.next;
                        over := true;
                    else
                        iterator := iterator.next;
                    end if;
                end loop;
            end if;
        else
            null;
        end if;
    end remove;

    procedure remove (l : in out p_node;
                      node : in p_node) is
        iterator : p_node;
        over : boolean;
    begin
        if l /= null then
            if l = node then
                l := l.next;
            else
                iterator := l;
                over := false;
                while iterator.next /= null and not over loop
                    if iterator.next = node then
                        iterator.next := iterator.next.next;
                        over := true;
                    else
                        iterator := iterator.next;
                    end if;
                end loop;
            end if;
        else
            null;
        end if;
    end remove;

    function equal (l1 : in p_node;
                  l2 : in p_node) return boolean is
    begin
        if l1 = null then
            if l2 = null then
                return true;
            else
                return false;
            end if;
        else
            if l2 = null then
                return false;
            else
                if l1.value = l2.value then
                    return equal(l1.next, l2.next);
                else
                    return false;
                end if;
            end if;
        end if;
    end equal;

    -- =====
    -- DEBUG
    -- =====
    procedure display (l : in p_node) is
        iterator : p_node;
    begin
        iterator := l;
        while iterator /= null loop
            put("("); put(image(iterator.value)); put(")");
            if has_next(iterator) then
                put("->");
            else
                null;
            end if;
            next(iterator);
        end loop;
        new_line;
    end display;
end linked_list;