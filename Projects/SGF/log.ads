with ada.text_io;
use ada.text_io;
with ada.integer_text_io;
use ada.integer_text_io;
with tools;
use tools;

package log is

    procedure error (msg : string);
    procedure error (msg : better_string);

    procedure info (msg : string);
    procedure info (msg : better_string);

    procedure debug (msg : string);
    procedure debug (msg : better_string);

    procedure warning (msg : string);
    procedure warning (msg : better_string);

end log;