with address;
use address;
with ada.text_io; use ada.text_io;
with ada.integer_text_io; use ada.integer_text_io;
with tools;

procedure test_address is

    procedure print_test_result (nb_tests_ok : in integer;
                                 nb_tests_error : in integer) is
    begin
        put(nb_tests_ok, 0);put("/");put(nb_tests_error+nb_tests_ok, 0);put(" tests ok");
        if nb_tests_error > 0 then
            put(", ");put(nb_tests_error, 0);put(" errors");
        end if;
        new_line;
    end print_test_result;

    procedure test_equal is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        addr_1 : t_address;
        addr_2 : t_address;

        procedure print_test_error (addr_1 : in t_address;
                                    addr_2 : in t_address) is
        begin
            put("Error: ");display(addr_1);put(" = ");display(addr_2);new_line;
        end print_test_error;
    begin
        put_line("test equal");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        -- 0 = 0
        init_address(addr_1, (others => 0));
        init_address(addr_2, (others => 0));
        if equal(addr_1, addr_2) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2);
        end if;

        -- 45G = 45G
        init_address(addr_1, (4 => 45, others => 0));
        init_address(addr_2, (4 => 45, others => 0));
        if equal(addr_1, addr_2) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2);
        end if;

        -- 1T /= 1G
        init_address(addr_1, (5 => 1, others => 0));
        init_address(addr_2, (4 => 1, others => 0));
        if equal(addr_1, addr_2) then
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2);
        else
            nb_tests_ok := nb_tests_ok + 1;
        end if;

        -- 1T 43G 111M 999K 165 == 1T 43G 111M 999K 165
        init_address(addr_1, (5 => 1, 4 => 43, 3 => 111, 2 => 999, 1 => 165, others => 0));
        init_address(addr_2, (5 => 1, 4 => 43, 3 => 111, 2 => 999, 1 => 165, others => 0));
        if equal(addr_1, addr_2) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2);
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_equal;

    procedure test_add is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        addr_1 : t_address;
        addr_2 : t_address;
        result : t_address;
        expected_result : t_address;

        procedure print_test_error (addr_1 : in t_address;
                                    addr_2 : in t_address;
                                    result : in t_address;
                                    expected_result : in t_address) is
        begin
            put("Error: ");display(addr_1);put(" + ");display(addr_2);new_line;
            put("Got: ");
            display(result);new_line;
            put("Expected: ");
            display(expected_result);
            new_line;
        end print_test_error;
    begin
        put_line("test add");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        -- 0 + 0
        init_address(addr_1, (others => 0));
        init_address(addr_2, (others => 0));
        result := add(addr_1, addr_2);
        init_address(expected_result, (others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 25 + 30 = 55
        init_address(addr_1, (1 => 25, others => 0));
        init_address(addr_2, (1 => 30, others => 0));
        result := add(addr_1, addr_2);
        init_address(expected_result, (1 => 55, others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 900 + 99 = 999
        init_address(addr_1, (1 => 900, others => 0));
        init_address(addr_2, (1 => 99, others => 0));
        result := add(addr_1, addr_2);
        init_address(expected_result, (1 => 999, others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 900 + 100 = 1K
        init_address(addr_1, (1 => 900, others => 0));
        init_address(addr_2, (1 => 100, others => 0));
        result := add(addr_1, addr_2);
        init_address(expected_result, (2 => 1, others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 900 + 150 = 1K 50
        init_address(addr_1, (1 => 900, others => 0));
        init_address(addr_2, (1 => 150, others => 0));
        result := add(addr_1, addr_2);
        init_address(expected_result, (1 => 50, 2 => 1, others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 1T 140G 0M 624K 976 + 3T 23G 431M 431K 1 = 4T 163G 432M 55K 977
        init_address(addr_1, (5 => 1, 4 => 140, 2 => 624, 1 => 976, others => 0));
        init_address(addr_2, (5 => 3, 4 => 23, 3 => 431, 2 => 431, 1 => 1, others => 0));
        result := add(addr_1, addr_2);
        init_address(expected_result, (5 => 4, 4 => 163, 3 => 432, 2 => 55, 1 => 977, others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 999T 999G 999M 999K 998 + 2 = address_overflow
        init_address(addr_1, (5 => 999, 4 => 999, 3 => 999, 2 => 999, 1 => 998, others => 0));
        init_address(addr_2, (1 => 2, others => 0));
        begin
            result := add(addr_1, addr_2);
            nb_tests_error := nb_tests_error + 1;
            put("Error: ");display(addr_1);put(" + ");display(addr_2);new_line;
            put("Got: ");
            display(result);new_line;
            put_line("Expected: RAISED address_overflow");
        exception
            when address_overflow => nb_tests_ok := nb_tests_ok + 1;
        end;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_add;

    procedure test_remove is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        addr_1 : t_address;
        addr_2 : t_address;
        result : t_address;
        expected_result : t_address;

        procedure print_test_error (addr_1 : in t_address;
                                    addr_2 : in t_address;
                                    result : in t_address;
                                    expected_result : in t_address) is
        begin
            put("Error: ");display(addr_1);put(" - ");display(addr_2);new_line;
            put("Got: ");
            display(result);new_line;
            put("Expected: ");
            display(expected_result);
            new_line;
        end print_test_error;
    begin
        put_line("test remove");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        -- 0 - 0 = 0
        init_address(addr_1, (others => 0));
        init_address(addr_2, (others => 0));
        result := remove(addr_1, addr_2);
        init_address(expected_result, (others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 100 - 54 = 46
        init_address(addr_1, (1 => 100, others => 0));
        init_address(addr_2, (1 => 54, others => 0));
        result := remove(addr_1, addr_2);
        init_address(expected_result, (1 => 46, others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 432 - 432 = 0
        init_address(addr_1, (1 => 432, others => 0));
        init_address(addr_2, (1 => 432, others => 0));
        result := remove(addr_1, addr_2);
        init_address(expected_result, (others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 45M 12K 148 - 44M 11K 147 = 1M 1K 1
        init_address(addr_1, (3 => 45, 2 => 12, 1 => 148, others => 0));
        init_address(addr_2, (3 => 44, 2 => 11, 1 => 147, others => 0));
        result := remove(addr_1, addr_2);
        init_address(expected_result, (3 => 1, 2 => 1, 1 => 1, others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 999T 999G 999M 999K 999 - 999T 999G 999M 999K = 999
        init_address(addr_1, (5 => 999, 4 => 999, 3 => 999, 2 => 999, 1 => 999, others => 0));
        init_address(addr_2, (5 => 999, 4 => 999, 3 => 999, 2 => 999, others => 0));
        result := remove(addr_1, addr_2);
        init_address(expected_result, (1 => 999, others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 1T - 5G = 995G
        init_address(addr_1, (5 => 1, others => 0));
        init_address(addr_2, (4 => 5, others => 0));
        result := remove(addr_1, addr_2);
        init_address(expected_result, (4 => 995, others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 1T - 1G 0M 0K 12 = 998G 999M 999K 988
        init_address(addr_1, (5 => 1, others => 0));
        init_address(addr_2, (4 => 1, 1 => 12, others => 0));
        result := remove(addr_1, addr_2);
        init_address(expected_result, (4 => 998, 3 => 999, 2 => 999, 1 => 988, others => 0));
        if equal(result, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2, result, expected_result);
        end if;

        -- 1T - 2T = RAISED address_negative
        init_address(addr_1, (5 => 1, others => 0));
        init_address(addr_2, (5 => 2, others => 0));
        begin
            result := remove(addr_1, addr_2);
            nb_tests_error := nb_tests_error + 1;
            put("Error: ");display(addr_1);put(" - ");display(addr_2);new_line;
            put("Got: ");
            display(result);new_line;
            put_line("Expected: RAISED address_negative");
        exception
            when address_negative => nb_tests_ok := nb_tests_ok + 1;
        end;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_remove;

    procedure test_is_zero is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        addr_1 : t_address;
    begin
        put_line("test is zero");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        -- 0
        init_address(addr_1, (others => 0));
        if is_zero(addr_1) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put("Error: ");display(addr_1);new_line;
            put_line("Should be zero");
        end if;

        -- 1
        init_address(addr_1, (1 => 1, others => 0));
        if not is_zero(addr_1) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put("Error: ");display(addr_1);new_line;
            put_line("Should be non zero");
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_is_zero;

    procedure test_less is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        addr_1 : t_address;
        addr_2 : t_address;

        procedure print_test_error (addr_1 : in t_address;
                                    addr_2 : in t_address) is
        begin
            put("Error: ");display(addr_1);put(" < ");display(addr_2);new_line;
        end print_test_error;
    begin
        put_line("test less");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        -- 1 < 2
        init_address(addr_1, (4 => 1, others => 0));
        init_address(addr_2, (5 => 1, others => 0));
        if less(addr_1, addr_2) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2);
        end if;

        -- 2 < 1
        init_address(addr_2, (4 => 1, others => 0));
        init_address(addr_1, (5 => 1, others => 0));
        if not less(addr_1, addr_2) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2);
        end if;

        -- 2 < 2
        init_address(addr_2, (5 => 1, others => 0));
        init_address(addr_1, (5 => 1, others => 0));
        if not less(addr_1, addr_2) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2);
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_less;

    procedure test_greater is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        addr_1 : t_address;
        addr_2 : t_address;

        procedure print_test_error (addr_1 : in t_address;
                                    addr_2 : in t_address) is
        begin
            put("Error: ");display(addr_1);put(" > ");display(addr_2);new_line;
        end print_test_error;
    begin
        put_line("test greater");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        -- 1 > 2
        init_address(addr_1, (4 => 1, others => 0));
        init_address(addr_2, (5 => 1, others => 0));
        if not greater(addr_1, addr_2) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2);
        end if;

        -- 2 > 1
        init_address(addr_2, (4 => 1, others => 0));
        init_address(addr_1, (5 => 1, others => 0));
        if greater(addr_1, addr_2) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2);
        end if;

        -- 2 > 2
        init_address(addr_2, (5 => 1, others => 0));
        init_address(addr_1, (5 => 1, others => 0));
        if not greater(addr_1, addr_2) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            print_test_error(addr_1, addr_2);
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_greater;

begin

    test_equal;
    test_add;
    test_remove;
    test_is_zero;
    test_less;
    test_greater;

end test_address;