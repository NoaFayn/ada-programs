with linked_list;
with tools; use tools;
with ada.text_io; use ada.text_io;
with ada.integer_text_io; use ada.integer_text_io;
with ada.calendar; use ada.calendar;
with log; use log;
with address; use address;
with disk; use disk;

package SGF is
    type t_sgf is private;
    type file is private;
    type p_file is access file;

    function image (f : p_file) return string;

    -- Use generic package
    package linked_list_file is new linked_list (p_file, image, "=");
    use linked_list_file;

    not_a_directory     : exception;
    file_already_exists : exception;
    invalid_argument    : exception;
    no_such_file        : exception;
    is_a_directory      : exception;
    cannot_delete_root  : exception;
    cannot_edit_root    : exception;

    -- Role: Checks if the specified path points to a valid file (or directory).
    -- Parameters: fs - file system to work on
    -- path - path to check
    -- Returns: TRUE if a file exsits at this path and FALSE otherwise
    -- Pre:
    -- Post:
    function exists (fs : in t_sgf;
                     path : in better_string) return boolean;


    -- Role: Initialise the file system manager. By doing so, the root is created and the current directory is set to the root.
    -- Parameter: fs - file system manager to initialise
    -- Pre:
    -- Post: the root of the file system is created and the current directory is set to this root
    procedure initialise_sgf (fs : out t_sgf);

    -- Role: Gets the current directory of the file system.
    -- Parameter: fs - file system to get the current directory of
    -- Returns: current directory of the file system
    -- Pre:
    -- Post: 
    function get_current_dir_name (fs : in t_sgf) return better_string;

    -- Role: Gets the current directory of the file system with the full path.
    -- Parameter: fs - file system to get the current directory of
    -- Returns: current directory of the file system
    -- Pre:
    -- Post:
    function get_current_dir_path (fs : in t_sgf) return better_string;

    -- Role: Creates a new file in the file system at the specified path.
    -- Parameters: fs - file system to create the file in
    -- path - path of the file to create (the last component of the path is the name of the file)
    -- Example: /home/u1/test.txt will create the file text.txt in the /home/u1 directory
    -- Other example: text.txt will create the file text.txt in the current directory of the file system
    -- Other example: ../text.txt will create the file text.txt in the parent of the current directory of the file system
    -- Pre: 
    -- Post: the file exists
    -- Exception: not_a_directory - if a component of the path is not a directory
    -- file_already_exists - if the specified path already exists
    procedure create_file (fs : in out t_sgf;
                           path : in better_string);

    -- Role: Updates the time of the file pointer by the specified path to the current date time.
    -- Parameters: fs - file system to update the file on
    -- path - path of the file to update
    -- Pre:
    -- Post: file timestamp is now
    -- Exception: not_a_directory - if a component of the path is not a directory
    -- no_such_file - if the specified path doesn't exist
    procedure update_file_timestamp (fs : in t_sgf;
                                     path : in better_string);

    -- Role: Changes the size of the specified file to its new size.
    -- Parameters: path - path of the file to edit
    -- newSize - new size of the file to set
    -- Pre: newSize > 0
    -- Post: the new size of the file is newSize
    -- Exceptions: not_a_directory - if a component of the path is not a directory
    -- no_such_file - if the specified path doesn't exist
    procedure change_file_size (fs : in out t_sgf;
                                path : in better_string;
                                newSize : in t_address);

    -- Role: Creates a new directory in the file system at the specified path.
    -- Parameters: fs - file system to create the directory in
    -- path - path of the directory to create (the last component of the path is the name of the directory)
    -- Example: /home/u1 will create the directory u1 in the directory /home
    -- Other example: u1 will create the directory u1 in the current directory of the file system
    -- Other example: ../u1 will create the directory u1 in the parent of the current directory of the file system
    -- Pre:
    -- Post: the directory exists
    -- Exception: not_a_directory - if a component of the path is not a directory
    -- file_already_exists - if the specified path already exists
    procedure create_dir (fs : in out t_sgf;
                          path : in better_string);

    -- Role: Sets the current directory to the specified path
    -- Parameters: fs - the file system to change the current directory of
    -- path - path for the current directory
    -- Pre:
    -- Post: the current directory of the file system is the path
    -- Exception: not_a_directory - if a component of the path is not a directory
    -- no_such_file - if the specified path doesn't exist
    procedure set_current_dir (fs : in out t_sgf;
                               path : in better_string);

    -- Role: Lists the content of the specified path with the following format:
    -- [f|d]<rights> <owner> <group> <size> <name>
    -- Parameters: fs - the file system to display the content of
    -- path - path to display the content of
    -- Pre:
    -- Post: 
    -- Exception: not_a_directory - if a component of the path is not a directory
    -- no_such_file - if the specified path doesn't exist
    procedure display_content (fs : in t_sgf;
                               path : in better_string);

    -- Role: Lists the content of the specified path along with the content of each subdirectory with the following format:
    -- \- path
    --  \- directory
    --      \- directory
    --          \- directory
    --              |- file
    --              |- file
    --          |- file
    --  \- directory
    -- Parameters: fs - the file system to work on
    -- path - path to start the recursion
    -- Pre:
    -- Post:
    -- Exception: not_a_directory - if a component of the path is not a directory
    -- no_such_file - if the specified path doesn't exist
    procedure display_content_recursive (fs : in t_sgf;
                                         path : in better_string);

    -- Role: Removes the specified file or directory from the file system.
    -- Parameters: fs - the file system to remove the file from
    -- path - path of the file to remove
    -- remove_only_file - if TRUE then only the actual files will be removed (and not the directories)
    -- Pre:
    -- Post: the file no longer exists in the file system
    -- Exception: not_a_directory - if a component of the path is not a directory
    -- no_such_file - if the specified path doesn't exist
    -- is_a_directory - if the path points to a directory instead of a file
    procedure remove_file (fs : in out t_sgf;
                           path : in better_string;
                           remove_only_file : in boolean);

    -- Role: Moves the source file to the destination path.
    -- Parameters: fs - the file system to move the file on
    -- path_source - path of the file to move
    -- path_dest - path of the destination file (the last component of the path is the name of the new file)
    -- Pre:
    -- Post: the file at path_source no longer exists and the file at path_dest is the same file that was at path_source with a name equal to the last component of the path_dest
    -- Exception: not_a_directory - if a component of a path is not a directory
    -- no_such_file - if the path_source doesn't exist
    procedure move_file (fs : in out t_sgf;
                         path_source : in better_string;
                         path_dest : in better_string);

    -- Role: Copies a file or directory from the path_source to the path_dest.
    -- Parameters: fs - the file system to work on
    -- path_source - path of the source to copy
    -- path_dest - path of the destination
    -- Pre:
    -- Post: the file at path_source is unchanged and the file at path_dest is the same file that was at path_source with a name equal to the last component of the path_dest
    -- Exception: not_a_directory - if a component of a path is not a directory
    -- no_such_file - if the path_source doesn't exist
    procedure copy_file (fs : in out t_sgf;
                    path_source : in better_string;
                    path_dest : in better_string);

    -- Role: Archives a directory which will create a file with a size equal to the sum of each files contained in the source directory and subdirectories.
    -- Parameters: fs - the file system to work on
    -- path_source - path of the directory to create the archive of
    -- path_dest - path of the archive
    -- Pre:
    -- Post: the file at path_dest is a file with the size equal to the sum of each file contained in the source directory and subdirectories named with the last component of the path_dest
    -- Exception: not_a_directory - if a component of the path is not a directory or if the source path is not a directory
    -- no_such_file - if the specified path doesn't exist
    -- file_already_exists - if the destination file already exists
    procedure archive (fs : in out t_sgf;
                       path_source : in better_string;
                       path_dest : in better_string);

    -- Role: Changes the current owner. This is the value that will be used for file access checks and file creation.
    -- Parameters: fs - file system to work on
    -- new_owner - new value for the owner of the files
    -- Pre:
    -- Post: the current owner of the fs is the new owner
    procedure change_current_owner (fs : in out t_sgf;
                                    new_owner : in better_string);

    -- Role: Changes the current group. This is the value that will be used for file access checks and file creation.
    -- Parameters: fs - file system to work on
    -- new_group - new value for the group of the files
    -- Pre:
    -- Post: the current group of the fs is the new group
    procedure change_current_group (fs : in out t_sgf;
                                    new_group : in better_string);

    private
        type file is record
            isDir : boolean;
            name : better_string;
            rights : integer;
            parent : p_file;
            content : linked_list_file.p_node;
            addr : t_address;
            size : t_address;
            date : time;
            owner : better_string;
            group : better_string;
        end record;
        type t_sgf is record
            hard_disk : t_disk;
            current_dir : p_file;
            root : p_file;
            current_owner : better_string;
            current_group : better_string;
        end record;


end SGF;