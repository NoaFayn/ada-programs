package body disk is

    -- =====
    -- TOOLS
    -- =====
    -- Role: Calculates the space of the block.
    -- Parameter: block - block to calculate the space of
    -- Pre:
    -- Post:
    function space_of_block (block : in t_block) return t_address is
    begin
        return address.remove(block.addr_high, block.addr_low);
    end space_of_block;

    -- Role: Checks if the specified address with the specified size is in the block boundaries.
    -- Parameters: block - block to check on
    -- addr - address to check
    -- size - size of the address to check
    -- Returns: TRUE if the address with its size is in the block boundaries and FALSE otherwise
    -- Pre:
    -- Post:
    function is_address_in_block (block : in t_block;
                               addr : in t_address;
                               size : in t_address) return boolean is
        addr_upper_limit : t_address;
    begin
        addr_upper_limit := address.add(addr, size);
        return not address.less(addr, block.addr_low) and not address.greater(addr_upper_limit, block.addr_high);
    end is_address_in_block;

    -- =======
    -- PACKAGE
    -- =======

    function image (elt : in t_block) return string is
    begin
        return "[" & tools.value(address.image(elt.addr_low)) & "|" & tools.value(address.image(elt.addr_high)) & "]";
    end image;

    function equal (b1 : in t_block; b2 : in t_block) return boolean is
    begin
        return address.equal(b1.addr_low, b2.addr_low) and address.equal(b1.addr_high, b2.addr_high);
    end equal;

    procedure init_disk (disk : out t_disk;
                         max_address : in t_address) is
        all_space : t_block;
        addr_low : t_address;
    begin
        init_address(addr_low, (others => 0));
        linked_list_block.init_list(disk.free_spaces);
        all_space := (addr_low => addr_low, addr_high => max_address);
        linked_list_block.insert_first(disk.free_spaces, all_space);
        disk.max_space := max_address;
    end init_disk;
    
    procedure alloc (disk : in out t_disk;
                     size : in t_address;
                     address_found : out t_address) is
        current_node : linked_list_block.p_node;
        current_block : t_block;
        block_space : t_address;
        total_free_space : t_address;
    begin
        -- Disk has no free space
        if linked_list_block.is_empty(disk.free_spaces) then
            raise disk_full;
        end if;

        -- Size is invalid
        if address.is_zero(size) then
            raise disk_wrong_size;
        end if;

        init_address(total_free_space, (others => 0));
        current_node := disk.free_spaces;
        current_block := linked_list_block.get_value(current_node);
        block_space := space_of_block(current_block);
        -- Find enough space to alloc
        while not address.is_zero(block_space) and less(block_space, size) loop
            total_free_space := address.add(total_free_space, block_space);
            linked_list_block.next(current_node);
            if not linked_list_block.is_empty(current_node) then
                current_block := linked_list_block.get_value(current_node);
                block_space := space_of_block(current_block);
            else
                init_address(block_space, (others => 0));
            end if;
        end loop;
        if address.is_zero(block_space) then
            -- Not enough space
            -- Check if total free space is good
            if greater(size, total_free_space) then
                -- Cannot compact, disk is full
                raise disk_full;
            else
                -- Can compact
                put_line("DEBUG: [Warning]: can compact disk, but not implemented for now...");
                -- TODO: COMPACT DISK (stop raising exception)
                raise disk_full;
            end if;
        else
            -- Found a spot
            address_found := current_block.addr_low;
            -- Allocate block
            current_block.addr_low := address.add(current_block.addr_low, size);
            linked_list_block.set_value(current_node, current_block);
        end if;
    end alloc;

    procedure free (disk : in out t_disk;
                    addr : in t_address;
                    size : in t_address) is
        addr_upper_limit : t_address;
        free_block : t_block;
        current_node : linked_list_block.p_node;
        current_block : t_block;
        inserted : boolean;
        previous_node : linked_list_block.p_node;
        previous_block : t_block;
    begin
        addr_upper_limit := address.add(addr, size);

        -- Check if address overflows
        if greater(addr, disk.max_space) or greater(addr_upper_limit, disk.max_space) then
            raise disk_overflow;
        end if;

        -- Create new free block
        free_block := (addr_low => addr, addr_high => addr_upper_limit);
        -- Insert free block in the disk
        current_node := disk.free_spaces;
        inserted := false;
        while not linked_list_block.is_empty(current_node) and not inserted loop
            current_block := linked_list_block.get_value(current_node);
            if less(free_block.addr_low, current_block.addr_low) then
                -- Insert block here
                linked_list_block.insert_before(disk.free_spaces, current_node, free_block);
                inserted := true;
            else
                -- Next block
                linked_list_block.next(current_node);
            end if;
        end loop;
        if not inserted then
            -- Insert at the end
            linked_list_block.insert_last(disk.free_spaces, free_block);
        end if;

        -- Simplify blocks
        previous_node := disk.free_spaces;
        current_node := linked_list_block.get_next(previous_node);
        while not linked_list_block.is_empty(current_node) loop
            previous_block := linked_list_block.get_value(previous_node);
            current_block := linked_list_block.get_value(current_node);
            -- Check that there is no overlap between the free blocks
            if address.less(current_block.addr_low, previous_block.addr_high) or address.equal(current_block.addr_low, previous_block.addr_high) then
                -- Simplify the blocks
                linked_list_block.remove(disk.free_spaces, previous_node);
                current_block.addr_low := previous_block.addr_low;
                linked_list_block.set_value(current_node, current_block);
            end if;

            previous_node := current_node;
            linked_list_block.next(current_node);
        end loop;
        -- TODO: blocks with same addr low and high!
    end free;

    function is_free_address (disk : in t_disk;
                              addr : in t_address;
                              size : in t_address) return boolean is
        current_node : linked_list_block.p_node;
        current_block : t_block;
    begin
        current_node := disk.free_spaces;
        -- Disk is full
        if linked_list_block.is_empty(current_node) then
            return false;
        end if;

        -- TODO: Can be smarter here for the loop stop
        while not linked_list_block.is_empty(current_node) loop
            current_block := linked_list_block.get_value(current_node);
            if is_address_in_block(current_block, addr, size) then
                return true;
            else
                -- Next block
                linked_list_block.next(current_node);
            end if;
        end loop;
        return false;
    end is_free_address;

    procedure display (disk : in t_disk) is
        block : t_block;
        current_node : linked_list_block.p_node;
    begin
        current_node := disk.free_spaces;
        while not linked_list_block.is_empty(current_node) loop
            block := linked_list_block.get_value(current_node);
            put("[");address.display(block.addr_low);put(" - ");address.display(block.addr_high);put("]  ");
            linked_list_block.next(current_node);
        end loop;
    end display;

end disk;