with ada.text_io;
use ada.text_io;

generic
    type g_element is private;
    with function image (s : g_element) return string;
    with function "=" (e1 : in g_element; e2 : in g_element) return boolean;

package linked_list is
    type p_node is private;

    -- Role: Initialises the list. The list is empty
    -- Parameter: l - list to initialise
    -- Pre:
    -- Post: the list is empty
    procedure init_list (l : out p_node);

    -- Role: Checks if the list is empty.
    -- Parameter: l - list to check
    -- Returns: TRUE if the list has no elements in it and FALSE otherwise
    -- Pre:
    -- Post:
    function is_empty (l : in p_node) return boolean;

    -- Role: Checks if the list has a next element.
    -- Parameter: l - list to check
    -- Returns: TRUE if the list has a next element and FALSE otherwise
    -- Pre: the list is not null
    -- Post:
    function has_next (l : in p_node) return boolean;

    -- Role: Goes to the next element of the list
    -- Parameter: l - list to set the next element of
    -- Pre: the list is not null
    -- Post: 
    procedure next (l : in out p_node);

    -- Role: Gets the value of the current node.
    -- Parameter: l - list to get the element of
    -- Returns: the value of the current element of the list
    -- Pre: the list is not null
    -- Post:
    function get_value (l : in p_node) return g_element;

    -- Role: Sets the value of the current node.
    -- Parameters: l - node to set the value of
    -- value - value to set
    -- Pre:
    -- Post: the node l has the value value
    procedure set_value (l : in out p_node;
                        value : in g_element);

    -- Role: Inserts a value at the end of the list.
    -- Parameters: l - list to insert the value into
    -- value - value to insert
    -- Pre:
    -- Post: the value is the last element of the list
    procedure insert_last (l : in out p_node;
                           value : in g_element);

    -- Role: Inserts a value at the start of the list
    -- Parameters: l - list to insert the value into
    -- value - value to insert
    -- Pre:
    -- Post: the value is the first element of the list
    procedure insert_first (l : in out p_node;
                            value : in g_element);
    
    -- Role: Inserts a value after the after node of the list.
    -- If after is null then this procedure is equivalent to insert_first.
    -- If after is the last element of the list l, then this procedure is equivalent to insert_last.
    -- Parameters: l - list to insert the value into
    -- after - node after which the value will be inserted
    -- value - value to insert
    -- Pre: after is a node of l
    -- Post: the value is inserted in the list after the after node
    procedure insert_after (l : in out p_node;
                            after : in out p_node;
                            value : in g_element);
    
    -- Role: Inserts a value before the before node of the list.
    -- If before is null then this procedure is equivalent to insert_last.
    -- If before is the first element of the list l, then this procedure is equivalent to insert_first.
    -- Parameters: l - list to insert the value into
    -- before - node before which the value will be inserted
    -- value - value to insert
    -- Pre: before is a node of l
    -- Post: the value is inserted in the list before the before node
    procedure insert_before (l : in out p_node;
                             before : in p_node;
                             value : in g_element);

    -- Role: Gets the next element of this list without switching to it
    -- Parameter: l - list to get the next element of
    -- Returns: next element in the list
    -- Pre: the list l is not null
    -- Post:
    function get_next (l : in p_node) return p_node;

    -- Role: Copies (shallow copy) the input list in the output list until from the start node to the last_element. If the last element is not in the list_in or is null, then the whole list is copied.
    -- Parameters: list_in - the input list that will be copied
    -- list_out - the output list that will contain the copies of the elements
    -- last_element - the last node in the input list that will be the last node in the output list
    -- Pre:
    -- Post: the list_out is the copy of the list_in
    procedure copy (list_in : in p_node;
                    list_out : out p_node;
                    last_element : in p_node);

    -- Role: Removes the specified element from the list
    -- Parameters: l - the list to remove the element from
    -- elt - the element to remove
    -- Pre:
    -- Post: the element elt is not in the list anymore
    procedure remove (l : in out p_node;
                      elt : in g_element);

    -- Role: Removes the specified node from the list
    -- Parameters: l - the list to remove the element from
    -- node - the node to remove
    -- Pre:
    -- Post: the node elt is not in the list anymore
    procedure remove (l : in out p_node;
                      node : in p_node);

    -- Role: Compares two lists and check if there are equal.
    -- Parameters: l1 - first list
    -- l2 - second list to check against
    -- Returns: TRUE if the lists are equal and FALSE otherwise
    -- Pre:
    -- Post: 
    function equal (l1 : in p_node;
                  l2 : in p_node) return boolean;

    -- DEBUG
    procedure display (l : in p_node);


    private
        type t_node;
        type p_node is access t_node;
        type t_node is record
            next : p_node;
            value : g_element;
        end record;
end linked_list;