package body tools is

    
    function "=" (s1, s2 : better_string) return boolean is
    begin
        if s1.size /= s2.size then
            return false;
        end if;
        return s1.str(1..s1.size) = s2.str(1..s1.size);
    end "=";

    function value (s : in better_string) return string is
    begin
        return s.str(1..s.size);
    end value;

    procedure print (s : in better_string) is
    begin
        put(value(s));
    end print;

    function split (s : in better_string;
                    separator : in character) return linked_list_string.p_node is
        result : linked_list_string.p_node;
        index : integer;
        part : better_string;
        part_index : integer;
    begin
        linked_list_string.init_list(result);
        index := 1;
        -- Read all the string
        while index <= s.size loop
            part := (str => (others => Character'Val(0)), size => 0);
            part_index := 1;
            -- Get the string part (until the separator is reached)
            while index <= s.size and s.str(index) /= separator loop
                part.str(part_index) := s.str(index);
                part_index := part_index + 1;
                part.size := part.size + 1;
                index := index + 1;
            end loop;
            -- Add the path component to the result
            linked_list_string.insert_last(result, part);
            -- Increment the index (to eliminate the separator)
            index := index + 1;
        end loop;

        return result;
    end split;

    function concat(list : in linked_list_string.p_node;
                    separator : in character) return better_string is
        iterator : linked_list_string.p_node;
        result : better_string;
        list_element : better_string;
    begin
        iterator := list;
        result := (str => (others => character'val(0)), size => 0);
        while not linked_list_string.is_empty(iterator) loop
            -- Append list element to the result
            list_element := linked_list_string.get_value(iterator);
            result.str(result.size+1..result.size+list_element.size) := value(list_element);
            result.size := result.size + list_element.size;
            -- Separator
            if linked_list_string.has_next(iterator) then
                result.str(result.size+1) := separator;
                result.size := result.size + 1;
            else
                null;
            end if;
            -- Next element
            linked_list_string.next(iterator);
        end loop;
        return result;
    end concat;

    procedure basedir (path : in better_string;
                      dir_path : out better_string;
                      file_name : out better_string) is
        path_list : tools.linked_list_string.p_node;
        path_list_iterator : tools.linked_list_string.p_node;
        dir_path_list : tools.linked_list_string.p_node;
    begin
        -- Path is /xxx/xxx/file_name
        -- or ./file_name
        -- or ../file_name
        -- or file_name

        -- First, get the directory in which the file will be created
        -- Split the path
        path_list := split(path, '/');

        path_list_iterator := path_list;
        -- Check if the path is just the name of the file or a real path
        if linked_list_string.has_next(path_list_iterator) then
            -- Find the parent directory from the path
            while linked_list_string.has_next(linked_list_string.get_next(path_list_iterator)) loop
                linked_list_string.next(path_list_iterator);
            end loop;
            -- We now have the path of the directory
            -- Copy the dir path
            linked_list_string.copy(path_list, dir_path_list, path_list_iterator);

            -- Concat the list path back to a string
            dir_path := concat(dir_path_list, '/');
            -- Get file name
            file_name := linked_list_string.get_value(linked_list_string.get_next(path_list_iterator));
        else
            -- One only element in the path
            -- Option 1:
            if linked_list_string.get_value(path_list_iterator).size = 0 then
                -- The path is the root
                -- Current directory is root
                dir_path := string_to_better("/", 1);
                -- File is root too
                file_name := string_to_better("/", 1);
            -- Option 2:
            else
                -- The path is just the name of the file
                -- Get the current directory
                dir_path := string_to_better(".", 1);
                -- Get file name
                file_name := path;
            end if;
        end if;
    end basedir;

    function string_to_better (s : in string; size : in integer) return better_string is
        bs : better_string;
    begin
        bs := (str => s & (size+1..MAX_STRING_LENGTH => character'val(0)), size => size);
        return bs;
    end string_to_better;

    procedure print_date_time (dt : in time) is
        type day_integer is range 0..86400;

        f_year : year_number;
        f_month : month_number;
        f_day : day_number;
        ignored : duration;
        f_total_seconds : day_integer;
        f_hours : natural;
        f_minutes : natural;
        f_seconds : natural;
    begin
        -- date
        ada.calendar.split(dt, f_year, f_month, f_day, ignored);
        put(f_year, 0);put("-");put(f_month, 0);put("-");put(f_day, 0);
        put(" ");
        -- time
        f_total_seconds := day_integer(ada.calendar.seconds(dt));
        f_seconds := natural(f_total_seconds rem 60);
        f_minutes := natural(f_total_seconds / 60) rem 60;
        f_hours := natural(f_total_seconds / 60) / 60;
        put(f_hours, 1);put(":");
        if f_minutes < 10 then
            put('0');
        end if;
        put(f_minutes, 1);
        
        put(":");
        if f_seconds < 10 then
            put('0');
        end if;
        put(f_seconds, 1);
    end print_date_time;
end tools;