with ada.text_io; use ada.text_io;
with ada.integer_text_io; use ada.integer_text_io;
with disk; use disk;
with address; use address;

procedure test_disk is

    procedure print_test_result (nb_tests_ok : in integer;
                                 nb_tests_error : in integer) is
    begin
        put(nb_tests_ok, 0);put("/");put(nb_tests_error+nb_tests_ok, 0);put(" tests ok");
        if nb_tests_error > 0 then
            put(", ");put(nb_tests_error, 0);put(" errors");
        end if;
        new_line;
    end print_test_result;

    procedure test_alloc (disk : in out t_disk) is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        size : t_address;
        memory_addr : t_address;
        expected_result : t_address;
    begin
        put_line("test alloc");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        -- Alloc 1G
        address.init_address(size, (4 => 1, others => 0));
        alloc(disk, size, memory_addr);
        address.init_address(expected_result, (others => 0));
        if address.equal(memory_addr, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: Alloc ");address.display(size);new_line;
            put("Got: ");address.display(memory_addr);new_line;
            put("Expected: ");address.display(expected_result);new_line;
        end if;

        -- Alloc 300M
        address.init_address(size, (3 => 300, others => 0));
        alloc(disk, size, memory_addr);
        address.init_address(expected_result, (4 => 1, others => 0));
        if address.equal(memory_addr, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: Alloc ");address.display(size);new_line;
            put("Got: ");address.display(memory_addr);new_line;
            put("Expected: ");address.display(expected_result);new_line;
        end if;

        -- Alloc 1T
        address.init_address(size, (5 => 1, others => 0));
        begin
            alloc(disk, size, memory_addr);
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: Alloc ");address.display(size);new_line;
            put("Got: ");address.display(memory_addr);new_line;
            put_line("Expected: RAISE disk_full");
        exception
            when disk_full => nb_tests_ok := nb_tests_ok + 1;
        end;

        -- Alloc 344G 12M 0K 875
        address.init_address(size, (4 => 344, 3 => 12, 1 => 875, others => 0));
        alloc(disk, size, memory_addr);
        address.init_address(expected_result, (4 => 1, 3 => 300, others => 0));
        if address.equal(memory_addr, expected_result) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: Alloc ");address.display(size);new_line;
            put("Got: ");address.display(memory_addr);new_line;
            put("Expected: ");address.display(expected_result);new_line;
        end if;

        -- Alloc 0
        address.init_address(size, (others => 0));
        begin
            alloc(disk, size, memory_addr);
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: Alloc ");address.display(size);new_line;
            put("Got: ");address.display(memory_addr);new_line;
            put_line("Expected: RAISE disk_wrong_size");
        exception
            when disk_wrong_size => nb_tests_ok := nb_tests_ok + 1;
        end;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_alloc;

    procedure test_free (disk : in out t_disk) is
        nb_tests_ok : integer;
        nb_tests_error : integer;

        addr : t_address;
        size : t_address;
    begin
        put_line("test free");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        -- Free 1G
        address.init_address(addr, (others => 0));
        address.init_address(size, (4 => 1, others => 0));
        free(disk, addr, size);
        if is_free_address(disk, addr, size) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put("Error: free ");address.display(size);put(" at addr: ");address.display(addr);new_line;
            put_line("Disk is not free");
        end if;

        -- Free 344G 12M 0K 875 at 1G 300M
        address.init_address(addr, (4 => 1, 3 => 300, others => 0));
        address.init_address(size, (4 => 344, 3 => 12, 1 => 875, others => 0));
        free(disk, addr, size);
        if is_free_address(disk, addr, size) then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put("Error: free ");address.display(size);put(" at addr: ");address.display(addr);new_line;
            put_line("Disk is not free");
            put("Disk free spaces: ");display(disk);new_line;
        end if;

        -- Not overflow
        address.init_address(addr, (5 => 1, others => 0));
        address.init_address(size, (others => 0));
        begin
            free(disk, addr, size);
            nb_tests_ok := nb_tests_ok + 1;
        exception
            when disk_overflow =>
                nb_tests_error := nb_tests_error + 1;
                put("Error: free ");address.display(size);put(" at addr: ");address.display(addr);new_line;
                put_line("Expected: not raising disk_overflow");
        end;

        -- Overflow
        address.init_address(addr, (5 => 1, others => 0));
        address.init_address(size, (1 => 1, others => 0));
        begin
            free(disk, addr, size);
            nb_tests_error := nb_tests_error + 1;
            put("Error: free ");address.display(size);put(" at addr: ");address.display(addr);new_line;
            put_line("Expected: RAISE disk_overflow");
        exception
            when disk_overflow =>
                nb_tests_ok := nb_tests_ok + 1;
        end;


        print_test_result(nb_tests_ok, nb_tests_error);
    end test_free;


    disk : t_disk;

    max_addr : t_address;

begin

    address.init_address(max_addr, (5 => 1, others => 0));
    init_disk(disk, max_addr);

    test_alloc(disk);
    -- The free test must be made AFTER the alloc test
    test_free(disk);

end test_disk;