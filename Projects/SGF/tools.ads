with ada.text_io;
use ada.text_io;
with ada.integer_text_io;
use ada.integer_text_io;
with ada.calendar;
use ada.calendar;

with linked_list;

package tools is

    MAX_STRING_LENGTH : constant integer := 1024;

    type better_string is record
        str : string(1..MAX_STRING_LENGTH);
        size : integer;
    end record;

    function "=" (s1, s2 : better_string) return boolean;

    -- Role: Gets the value as string type.
    -- Parameter: s - string
    -- Pre:
    -- Post:
    function value (s : in better_string) return string;

    package linked_list_string is new linked_list (better_string, value, "=");
    use linked_list_string;

    -- Role: Prints the string to the standard output. Warning: no new_line is made after the print.
    -- Parameter: s - the string to print
    -- Pre: the size of the string is between 1 and MAX_STRING_LENGTH
    -- Post:
    procedure print (s : in better_string);

    -- Role: Splits each part of the string separated by the separator.
    -- Parameter: s - string to split
    -- separator - character used to know how to split the string
    -- Returns: list of all the splitted parts
    -- Pre:
    -- Post:
    function split (s : in better_string;
                    separator : in character) return linked_list_string.p_node;

    -- Role: Concatenates each string of the list separated by the separator. This function does the inverse of the split function. Example: ["1", "2", "3"] with '/' separator will return: "1/2/3".
    -- Parameters: list - list of each string to concatenate
    -- separator - separator that will be appened to each string of the list
    -- Returns: one string with each string of the list separated by the separator
    -- Pre:
    -- Post:
    function concat (list : in linked_list_string.p_node;
                    separator : in character) return better_string;

    -- Role: Splits the given path into the directory path and the file name.
    -- For example, if the path is "/home/test.txt" then dir_path is "/home" and file_name is "test.txt".
    -- Or if path is "test.txt" then dir_path is "." and file_name is "test.txt".
    -- Edge case: if the path is "/", then the dir_path is "/" and the file_name is "/" too
    -- Parameters: path - path to split
    -- dir_path - path of the directory of the file pointed by the path
    -- file_name - name of the file pointed by the path
    -- Pre:
    -- Post: dir_path is "." if the path is only the file name and is the directory part of the path if the path is a full path and file_name is always the name of the file pointed by the path.
    procedure basedir (path : in better_string;
                      dir_path : out better_string;
                      file_name : out better_string);

    -- Role: Converts a string to a better string.
    -- Parameters: s - string to convert
    -- size - number of characters of the string
    -- Returns: the better string
    -- Pre: size <= MAX_STRING_LENGTH
    -- Post:
    function string_to_better (s : in string; size : in integer) return better_string;

    -- Role: Prints to the standard output the date time at this format: YEAR-MONTH-DAY HOURS:MINUTES:SECONDS
    -- Parameters: dt - time to display
    -- Pre:
    -- Post:
    procedure print_date_time (dt : in time);
end tools;