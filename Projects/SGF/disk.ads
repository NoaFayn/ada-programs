with ada.text_io; use ada.text_io;
with ada.integer_text_io; use ada.integer_text_io;
with address; use address;
with linked_list;
with tools;

package disk is
    type t_disk is private;

    -- t_block not private => to allow use in the generic linked_list_block
    type t_block is record
        addr_low : t_address;
        addr_high : t_address;
    end record;

    -- Role: Provides a string representation of a block
    -- Parameter: elt - block to get the representation of
    -- Returns: string representation of the block
    -- Pre:
    -- Post:
    function image (elt : in t_block) return string;
    -- Role: Checks if two blocks are equal. Two blocks are equal if both low and high addresses are the same.
    -- Parameters: b1 - first block to check
    -- b2 - second block to check
    -- Returns: TRUE if the blocks are equals and FALSE otherwise
    -- Pre:
    -- Post:
    function equal (b1 : in t_block; b2 : in t_block) return boolean;

    -- Instantiate generic linked list
    package linked_list_block is new linked_list (t_block, image, equal);
    use linked_list_block;

    disk_full           : exception;
    disk_overflow       : exception;
    disk_wrong_size     : exception;

    -- Role: Initialises the disk with a maximum address.
    -- Parameters: disk - disk to initialise
    -- max_address - max size of the disk
    -- Pre:
    -- Post:
    procedure init_disk (disk : out t_disk;
                         max_address : in t_address);
    
    -- Role: Allocates data on the disk.
    -- Parameters: disk - disk to store the data on
    -- size - size of the data to store
    -- address_found - address on the disk where the data is stored. If the disk_full exception is raised, then this value is meaningless.
    -- Pre:
    -- Post:
    -- Exception: disk_full - if there is not enough free space on the disk
    -- disk_wrong_size - if the size to allocate is 0
    procedure alloc (disk : in out t_disk;
                     size : in t_address;
                     address_found : out t_address);

    -- Role: Frees the data on the disk located at the specified address.
    -- Parameters: disk - disk to free the space of
    -- address - address of the disk to free
    -- size - size of the disk to free
    -- Pre:
    -- Post:
    -- Exception: disk_overflow - if the address is not on the disk capacity or if the size overflows the disk capacity
    procedure free (disk : in out t_disk;
                    addr : in t_address;
                    size : in t_address);

    -- Role: Checks if the specified address on the disk is free.
    -- Parameters: disk - disk to check on
    -- addr - address to check
    -- size - size of the address
    -- Returns: TRUE if the address is free up to its size and FALSE otherwise
    -- Pre:
    -- Post:
    function is_free_address (disk : in t_disk;
                              addr : in t_address;
                              size : in t_address) return boolean;
    
    -- DEBUG
    procedure display (disk : in t_disk);

    private
        type t_disk is record
            free_spaces : linked_list_block.p_node;
            max_space : t_address;
        end record;

end disk;