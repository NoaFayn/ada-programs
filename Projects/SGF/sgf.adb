package body SGF is

    -- =========
    -- | Tools |
    -- =========

    function image (f : p_file) return string is
    begin
        return tools.value(f.name);
    end image;

    -- Clonning a file will alloc new address !
    function clone (fs : in out t_sgf;
                    f : in p_file;
                    new_parent : in p_file;
                    new_name : in better_string) return p_file is
        cloned_file : p_file;
        file_addr : t_address;

        cloned_content : linked_list_file.p_node;
        content_iterator : linked_list_file.p_node;
    begin
        -- Alloc new address (size is the same as the cloned file)
        disk.alloc(fs.hard_disk, f.size, file_addr);
        -- First part of the instatiation
        -- Content is still missing
        linked_list_file.init_list(cloned_content);
        cloned_file := new file'(
            isDir => f.isDir,
            name => new_name,
            rights => f.rights,
            parent => new_parent,
            content => cloned_content,
            addr => file_addr,
            size => f.size,
            date => f.date,
            owner => f.owner,
            group => f.group
        );
        -- Recursively clone content
        content_iterator := f.content;
        while not linked_list_file.is_empty(content_iterator) loop
            -- Clone content
            linked_list_file.insert_last(cloned_content, clone(fs, linked_list_file.get_value(content_iterator), cloned_file, f.name));
            -- Next content
            linked_list_file.next(content_iterator);
        end loop;

        -- Second part of the instantiation: set the cloned content
        -- TODO: maybe not needed, because it is a pointer
        cloned_file.content := cloned_content;

        return cloned_file;
    end clone;

    -- Role: Calculates the size of the file. If the file is a directory, then the operation is recursive.
    -- Parameter: f - file or directory to get the size of
    -- Returns: total size of the file (recursively if directory)
    -- Pre:
    -- Post:
    function calculate_size_recursive (f : in p_file) return t_address is
        total_size : t_address;
        current_child : linked_list_file.p_node;
    begin
        if not f.isDir then
            total_size := f.size;
        else
            total_size := f.size;
            current_child := f.content;
            while not linked_list_file.is_empty(current_child) loop
                total_size := address.add(total_size, calculate_size_recursive(linked_list_file.get_value(current_child)));
                linked_list_file.next(current_child);
            end loop;
        end if;
        return total_size;
    end calculate_size_recursive;
    

    -- Role: Display a string representing the file as followed:
    -- [f|d]<rights> <owner> <group> <size> <name>
    -- Parameter: f - file to display
    -- Pre:
    -- Post:
    procedure display_file (f : in p_file) is
    begin
        -- [f|d]
        if f.isDir then
            put("d");
        else
            put("f");
        end if;
        -- rights
        put(f.rights, 0);
        put(" ");
        -- owner
        print(f.owner);
        put(" ");
        -- group
        print(f.group);
        put(" ");
        -- size
        put(tools.value(address.to_bs(f.size)));
        put(" ");
        -- date time
        tools.print_date_time(f.date);
        put(" ");
        -- name
        print(f.name);
        new_line;
    end display_file;

    -- Role: Gets the child with the specified name. This function handles the special names '.' and '..' and will return respectively the current directory dir or the current directory parent.
    -- Parameters: f - file to get the child of
    -- name - name of the child to get
    -- Returns: file with the specified name
    -- Pre: f is not null
    -- Post:
    -- Exception: no_such_file - if no file with the specified name exists
    -- not_a_directory - if the file is not a directory
    function get_child (dir : in p_file;
                        name : in better_string) return p_file is
        current_elt : linked_list_file.p_node;
        current_file : p_file;
        found : boolean;
        name_size : integer;
    begin
        -- Ensure that the file is a directory
        if not dir.isDir then
            raise not_a_directory;
        end if;

        found := false;
        current_elt := dir.content;

        -- Special name meaning
        -- '.' is current dir
        if name.size = 1 and then name.str(1..1) = "." then
            found := true;
            current_file := dir;
        
        -- '..' is parent
        elsif name.size = 2 and then name.str(1..2) = ".." then
            found := true;
            -- If we are at the root ('/') then the parent is itself
            if dir.parent = null then
                current_file := dir;
            -- Else the parent is the parent
            else
                current_file := dir.parent;
            end if;
        
        else
            -- Go through each file contained by the directory
            while not linked_list_file.is_empty(current_elt) and not found loop
                current_file := linked_list_file.get_value(current_elt);
                name_size := current_file.name.size;
                -- Check that file name matches
                if current_file.name = name then
                    found := true;
                else
                    linked_list_file.next(current_elt);
                end if;
            end loop;
        end if;

        -- Return value if found
        if not found then
            raise no_such_file;
        else
            return current_file;
        end if;
    end get_child;

    -- Role: Checks if the directory has a child with the specified name.
    -- Parameters: f - file to check
    -- name - name of the child to get
    -- Returns: TRUE if the directory has the file with the specified name and FALSE otherwise
    -- Pre: f is not null
    -- Post: 
    -- Exception: not_a_directory - if the file is not a directory
    function has_child (f : in p_file;
                        name : in better_string) return boolean is
        unused : p_file;
    begin
        unused := get_child(f, name);
        return true;
    exception
        when no_such_file => return false;
    end has_child;

    -- Role: Extracts the values of the path. For example: /home/u1/test.txt will return the list [/, home, u1, test.txt].
    -- Parameter: path - path to extract the values of
    -- Returns: list of the path values
    -- Pre:
    -- Post:
    function extract_path_values (path : in better_string) return tools.linked_list_string.p_node is

    begin
        return split(path, '/');
    end extract_path_values;

    -- Role: Creates a file in the specified directory with the specified name. During the creation, the owner of the file is set, the rights of the file are set, the date of the file is set to the current date.
    -- Parameters: dir - directory to create the file in
    -- name - name of the file to create
    -- Pre: dir is a directory
    -- Post: a file with the specified name exists in the directory dir
    procedure create_file (fs : in out t_sgf;
                           dir : in out p_file;
                           name : in better_string) is
        new_file : p_file;
        new_list : linked_list_file.p_node;
        file_addr : t_address;
        file_size : t_address;
    begin
        -- Default file size is 1 (because 0 is not allowed)
        address.init_address(file_size, (1 => 1, others => 0));
        -- Get file address
        disk.alloc(fs.hard_disk, file_size, file_addr);
        -- File has empty list of contained files
        linked_list_file.init_list(new_list);
        -- Create file
        new_file := new file'(isDir => false,
                             name => name,
                             rights => 0,
                             parent => dir,
                             content => new_list,
                             addr => file_addr,
                             size => file_size,
                             date => clock,
                             owner => fs.current_owner,
                             group => fs.current_group
        );
        linked_list_file.insert_last(dir.content, new_file);
    end create_file;

    -- Role: Creates a directory in the specified directory with the specified name. During the creation, the owner of the file is set, the rights of the file are set, the date of the file is set to the current date.
    -- Parameters: dir - directory to create the file in
    -- name - name of the directory to create
    -- Pre: dir is a directory
    -- Post: a directory with the specified name exists in the directory dir
    procedure create_dir ( fs : in out t_sgf;
                           dir : in out p_file;
                           name : in better_string) is
        new_dir : p_file;
        new_list : linked_list_file.p_node;
        dir_addr : t_address;
        dir_size : t_address;
    begin
        -- Directory has a default size of 10K
        address.init_address(dir_size, (2 => 10, others => 0));
        -- Allocate space for directory
        disk.alloc(fs.hard_disk, dir_size, dir_addr);
        -- Init contained files to empty list
        linked_list_file.init_list(new_list);
        -- Create directory
        new_dir := new file'(isDir => true,
                             name => name,
                             rights => 0,
                             parent => dir,
                             content => new_list,
                             addr => dir_addr,
                             size => dir_size,
                             date => clock,
                             owner => fs.current_owner,
                             group => fs.current_group
        );
        linked_list_file.insert_last(dir.content, new_dir);
    end create_dir;

    -- Role: Gets the file associated with the specified path.
    -- Parameters: fs - file system to work on
    -- path - path of the file to find
    -- Returns: file at the specified path
    -- Pre:
    -- Post:
    -- Exception: no_such_file - if no file exists at the specified path
    -- not_a_directory - if at some point in the path, it is not a directory
    function get_file (fs : in t_sgf;
                        path : in better_string) return p_file is
        path_values : tools.linked_list_string.p_node;
        str_component : better_string;
        current_dir : p_file;
        stop : boolean;
    begin
        path_values := extract_path_values(path);
        str_component := tools.linked_list_string.get_value(path_values);
        -- Choose start directory
        -- Absolute path
        if str_component.size = 0 then
            -- Start at the root (absolute path)
            current_dir := fs.root;
            -- Pass the root component and grab the next one
            tools.linked_list_string.next(path_values);
            if not tools.linked_list_string.is_empty(path_values) then
                str_component := tools.linked_list_string.get_value(path_values);
            else
                null;
            end if;
        -- Relative path
        else
            -- Start at the current directory
            current_dir := fs.current_dir;
            str_component := tools.linked_list_string.get_value(path_values);
        end if;

        -- Go down the tree to find the emplacement of the file
        stop := false;
        while not tools.linked_list_string.is_empty(path_values) and not stop loop
            -- Check if this component is the last (and so, the name of the file)
            if not tools.linked_list_string.has_next(path_values) then
                -- Found the file location!
                current_dir := get_child(current_dir, str_component);
                stop := true;
            else
                -- Not the filename yet, continue going down the tree
                begin
                    current_dir := get_child(current_dir, str_component);
                    tools.linked_list_string.next(path_values);
                    str_component := tools.linked_list_string.get_value(path_values);
                exception
                    when no_such_file =>
                        -- A component of the path is not a directory (doesn't exist)
                        raise not_a_directory;
                    when not_a_directory =>
                        -- A component of the path is not a directory (is a file)
                        raise not_a_directory;
                end;
            end if;
        end loop;

        return current_dir;
    end get_file;

    -- Role: Checks if the specified path points to a file.
    -- Parameters: fs - file system to work on
    -- path - path to check
    -- Returns: TRUE if the path points to an actual file and FALSE if the file doesn't exist or if the file is a directory
    -- Pre:
    -- Post:
    function is_file (fs : in t_sgf;
                      path : in better_string) return boolean is
        f : p_file;
    begin
        f := get_file(fs, path);
        return not f.isDir;
    exception
        when no_such_file => return false;
        when not_a_directory => return false;
    end is_file;

    -- Role: Checks if the specified path points to a directory.
    -- Parameters: fs - file system to work on
    -- path - path to check
    -- Returns: TRUE if the path points to an actual directory and FALSE if the file doesn't exist or if the file is an actual file (and not a directory)
    -- Pre:
    -- Post:
    -- Exception: not_a_directory - if at some point in the path, it is not a directory
    function is_directory (fs : in t_sgf;
                      path : in better_string) return boolean is
        f : p_file;
    begin
        f := get_file(fs, path);
        return f.isDir;
    exception
        when no_such_file => return false;
        when not_a_directory => return false;
    end is_directory;

    -- Role: Lists the content of the specified path along with the content of each subdirectory with the following format:
    -- \- path
    --  \- directory
    --      \- directory
    --          \- directory
    --              |- file
    --              |- file
    --          |- file
    --  \- directory
    -- Parameters: fs - the file system to work on
    -- file - file to display
    -- deep - the depth the display is made
    -- Pre:
    -- Post:
    -- Exception: not_a_directory - if a component of the path is not a directory
    -- no_such_file - if the specified path doesn't exist
    procedure display_content_recursive (fs : in t_sgf;
                                         file : in p_file;
                                         deep : in integer) is
        iterator : linked_list_file.p_node;
    begin
        -- Tabulation
        for i in 1..deep loop
            put("  ");
        end loop;

        if file.isDir then
            put("\- ");
            put(tools.value(file.name));
            new_line;
            -- Display all childs of this dir
            iterator := file.content;
            while not linked_list_file.is_empty(iterator) loop
                display_content_recursive(fs, linked_list_file.get_value(iterator), deep+1);
                linked_list_file.next(iterator);
            end loop;
        else
            put("|- ");
            put(tools.value(file.name));
            new_line;
        end if;
    end display_content_recursive;


    -- ===========
    -- | Package |
    -- ===========
    function exists (fs : in t_sgf;
                     path : in better_string) return boolean is
        ignored : p_file;
    begin
        begin
            ignored := get_file(fs, path);
            return true;
        exception
            when others => return false;
        end;
    end exists;

    procedure initialise_sgf (fs : out t_sgf) is
        root : p_file;
        new_list : linked_list_file.p_node;

        hard_disk : t_disk;
        max_disk_size : t_address;

        root_owner : better_string;
        root_size : t_address;
        root_addr : t_address;
    begin
        -- Init disk of 1T
        address.init_address(max_disk_size, (5 => 1, others => 0));
        disk.init_disk(hard_disk, max_disk_size);
        -- Init root size to 10K (its a directory)
        address.init_address(root_size, (2 => 10, others => 0));
        -- Allocate address for root
        disk.alloc(hard_disk, root_size, root_addr);
        root_owner := string_to_better("root", 4);
        linked_list_file.init_list(new_list);
        root := new file'(isDir => true,
                          name => (str => (1..MAX_STRING_LENGTH => Character'Val(0)), size => 1),
                          rights => 0,
                          parent => null,
                          content => new_list,
                          addr => root_addr,
                          size => root_size,
                          date => clock,
                          group => root_owner,
                          owner => root_owner
        );
        fs := (current_dir => root,
               root => root,
               current_owner => root_owner,
               current_group => root_owner,
               hard_disk => hard_disk
        );
    end initialise_sgf;

    function get_current_dir_name (fs : in t_sgf) return better_string is
    begin
        return fs.current_dir.name;
    end get_current_dir_name;

    function get_current_dir_path (fs : in t_sgf) return better_string is
        path : tools.linked_list_string.p_node;
        current_dir : p_file;
        result : better_string;
    begin
        tools.linked_list_string.init_list(path);
        if fs.current_dir = fs.root then
            -- Current dir is the root
            result := (str => "/" & (2..MAX_STRING_LENGTH => character'val(0)), size => 1);
        else
            -- Go back to the root
            current_dir := fs.current_dir;
            while current_dir /= fs.root loop
                tools.linked_list_string.insert_first(path, current_dir.name);
                current_dir := current_dir.parent;
            end loop;
            -- Add the root
            tools.linked_list_string.insert_first(path, fs.root.name);
            -- Convert to string
            result := tools.concat(path, '/');
        end if;

        return result;
    end get_current_dir_path;

    procedure create_file (fs : in out t_sgf;
                           path : in better_string) is
        parent : p_file;
        dir_path : better_string;
        file_name : better_string;
    begin
        -- Split base dir and file name
        tools.basedir(path, dir_path, file_name);
        parent := get_file(fs, dir_path);
        
        -- Check a file with this name doesn't already exist
        if has_child(parent, file_name) then
            raise file_already_exists;
        else
            -- Create the file
            create_file(fs, parent, file_name);
        end if;
    end create_file;

    procedure update_file_timestamp (fs : in t_sgf;
                                     path : in better_string) is
        f : p_file;
    begin
        f := get_file(fs, path);

        f.date := clock;
    end update_file_timestamp;

    procedure change_file_size (fs : in out t_sgf;
                                f : in p_file;
                                newSize : in t_address) is
        diff_size : t_address;
    begin
        -- Case 1: Same size
        if address.equal(f.size, newSize) then
            -- Do nothing
            null;
        -- Case 2: Change to smaller size
        elsif address.less(newSize, f.size) then
            -- Free only the diff size
            diff_size := address.remove(f.size, newSize);
            disk.free(fs.hard_disk, newSize, diff_size);
            -- Update size of file
            f.size := newSize;
            -- Address of file doesn't change
        -- Case 3: Change to bigger size
        else
            -- Realloc
            -- First free the previous location
            disk.free(fs.hard_disk, f.addr, f.size);
            -- Alloc to new location (= address of file is changed)
            disk.alloc(fs.hard_disk, newSize, f.addr);
            -- Update file size
            f.size := newSize;
        end if;
    end change_file_size;

    procedure change_file_size (fs : in out t_sgf;
                                path : in better_string;
                                newSize : in t_address) is
        f : p_file;
    begin
        f := get_file(fs, path);
        change_file_size(fs, f, newSize);
    end change_file_size;

    procedure create_dir (fs : in out t_sgf;
                          path : in better_string) is
        dir_path : better_string;
        dir_name : better_string;
        parent : p_file;
    begin
        -- Split base dir and dir name
        tools.basedir(path, dir_path, dir_name);
        parent := get_file(fs, dir_path);
        
        -- Check a directory with this name doesn't already exist
        if has_child(parent, dir_name) then
            raise file_already_exists;
        else
            -- Create the directory
            create_dir(fs, parent, dir_name);
        end if;
    end create_dir;

    procedure set_current_dir (fs : in out t_sgf;
                               path : in better_string) is
    begin
        fs.current_dir := get_file(fs, path);
    end set_current_dir;

    procedure display_content (fs : in t_sgf;
                               path : in better_string) is
        f : p_file;
        children : linked_list_file.p_node;
        child : p_file;
        total_children : integer;
    begin
        -- Get file
        f := get_file(fs, path);
        if f.isDir then
            -- Display the content of the folder
            total_children := 0;
            children := f.content;
            while not linked_list_file.is_empty(children) loop
                child := linked_list_file.get_value(children);
                display_file(child);
                linked_list_file.next(children);
                total_children := total_children + 1;
            end loop;
            put("total "); put(total_children, 0);
            new_line;
        else
            -- Display the file itself
            display_file(f);
        end if;
    end display_content;

    procedure display_content_recursive (fs : in t_sgf;
                                         path : in better_string) is
    begin
        display_content_recursive(fs, get_file(fs, path), 0);
    end display_content_recursive;

    procedure remove_file (fs : in out t_sgf;
                           file_to_remove : in p_file;
                           remove_only_file : in boolean) is
        parent : p_file;
        content_iterator : linked_list_file.p_node;
    begin
        parent := file_to_remove.parent;
        -- Do not remove the root "/"
        if parent = null then
            raise cannot_delete_root;
        else
            -- Check file to remove is actual file
            if remove_only_file and file_to_remove.isDir then
                raise is_a_directory;
            else
                linked_list_file.remove(parent.content, file_to_remove);
                -- Free disk space used by file
                disk.free(fs.hard_disk, file_to_remove.addr, file_to_remove.size);
                if file_to_remove.isDir then
                    -- Recursively free disk space used by content
                    content_iterator := file_to_remove.content;
                    remove_file(fs, linked_list_file.get_value(content_iterator), remove_only_file);
                    -- Next content
                    linked_list_file.next(content_iterator);
                end if;
            end if;
        end if;
    end remove_file;

    procedure remove_file (fs : in out t_sgf;
                           path : in better_string;
                           remove_only_file : in boolean) is
        file_to_remove : p_file;
    begin
        file_to_remove := get_file(fs, path);
        remove_file(fs, file_to_remove, remove_only_file);
    end remove_file;

    procedure move_file (fs : in out t_sgf;
                         path_source : in better_string;
                         path_dest : in better_string) is
        f_src : p_file;
        path_dir_dest : better_string;
        file_name_dest : better_string;
        parent_dest : p_file;
    begin
        f_src := get_file(fs, path_source);

        -- Check source is root
        if f_src = fs.root then
            raise cannot_edit_root;
        end if;

        -- Get destination directory
        if is_directory(fs, path_dest) then
        -- Destination is the given path
            parent_dest := get_file(fs, path_dest);
            file_name_dest := f_src.name;
        else
        -- Destination is the base directory of the given path
            -- Get destination parent and file name
            tools.basedir(path_dest, path_dir_dest, file_name_dest);
            -- Handle case where the parent is the root
            if path_dir_dest.size = 0 then
                path_dir_dest.str(1) := '/';
                path_dir_dest.size := 1;
            else
                null;
            end if;
            parent_dest := get_file(fs, path_dir_dest);
        end if;

        -- Check destination has no file with this name
        if has_child(parent_dest, file_name_dest) then
            raise file_already_exists;
        else
            -- Remove from parent directory
            linked_list_file.remove(f_src.parent.content, f_src);

            -- Move file to its destination
            linked_list_file.insert_last(parent_dest.content, f_src);

            -- Change name of dest file
            f_src.name := file_name_dest;
            -- Change parent of dest file
            f_src.parent := parent_dest;
        end if;

    end move_file;

    procedure copy_file (fs : in out t_sgf;
                    path_source : in better_string;
                    path_dest : in better_string) is
        f_src : p_file;
        path_dir_dest : better_string;
        file_name_dest : better_string;
        parent_dest : p_file;
        new_file : p_file;
    begin
        f_src := get_file(fs, path_source);

        -- Get destination directory
        if is_directory(fs, path_dest) then
        -- Destination is the given path
            parent_dest := get_file(fs, path_dest);
            file_name_dest := f_src.name;
        else
        -- Destination is the base directory of the given path
            -- Get destination parent and file name
            tools.basedir(path_dest, path_dir_dest, file_name_dest);
            -- Handle case where the parent is the root
            if path_dir_dest.size = 0 then
                path_dir_dest.str(1) := '/';
                path_dir_dest.size := 1;
            else
                null;
            end if;
            parent_dest := get_file(fs, path_dir_dest);
        end if;

        -- Check destination has no file with this name
        if has_child(parent_dest, file_name_dest) then
            raise file_already_exists;
        else
            -- Clone file
            -- clonning will allocate new disk address
            new_file := clone(fs, f_src, parent_dest, file_name_dest);

            -- Move clone file to its destination
            linked_list_file.insert_last(parent_dest.content, new_file);
        end if;

    end copy_file;

    procedure archive (fs : in out t_sgf;
                       path_source : in better_string;
                       path_dest : in better_string) is
        f_src : p_file;
        f_dest : p_file;
        parent_path_dest : better_string;
        file_name_dest : better_string;
        parent_dest : p_file;

        total_size : t_address;
    begin
        f_src := get_file(fs, path_source);
        -- Ensure the source is a directory
        if not f_src.isDir then
            raise not_a_directory;
        else
            -- Get parent destination directory and file destination name
            tools.basedir(path_dest, parent_path_dest, file_name_dest);
            parent_dest := get_file(fs, parent_path_dest);
            -- Check name is free to use
            if has_child(parent_dest, file_name_dest) then
                raise file_already_exists;
            else
                -- Create file
                create_file(fs, parent_dest, file_name_dest);
                f_dest := get_file(fs, path_dest);
                -- Calculate sum of sizes
                total_size := calculate_size_recursive(f_src);
                -- Update size to match sum
                change_file_size(fs, f_dest, total_size);
            end if;
        end if;
    end archive;

    procedure change_current_owner (fs : in out t_sgf;
                                    new_owner : in better_string) is
    begin
        fs.current_owner := new_owner;
    end change_current_owner;

    procedure change_current_group (fs : in out t_sgf;
                                    new_group : in better_string) is
    begin
        fs.current_group := new_group;
    end change_current_group;
end SGF;