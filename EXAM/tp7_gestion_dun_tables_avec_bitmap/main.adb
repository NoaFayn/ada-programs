
with ada.Text_IO, ada.Integer_Text_IO, ada.Float_Text_IO;
use ada.Text_IO, ada.Integer_Text_IO, ada.Float_Text_IO;

procedure Main is

   NMAX : constant integer := 30; -- maximum size of the array

   type tab_float is array (1..NMAX) of float;
   type bitmap_boolean is array (1..NMAX) of boolean;

   type bitmap_array is record
      tab : tab_float;
      bitmap : bitmap_boolean;
      imin : integer;
      imax : integer;
   end record;


   function nb_useful_elements(tab : in bitmap_array) return integer is
      total : integer;
   begin
      total := 0;
      for i in tab.imin..tab.imax loop
         if tab.bitmap(i) then
            total := total + 1;
         end if;
      end loop;
      return total;
   end nb_useful_elements;



   -- Role: Adds a value to the array
   -- Parameters: tab - array where the element will be added
   -- value - value to add in the array
   -- Pre: the array must have an empty slot to receive the value
   -- Post: the value is inserted in the array
   procedure add_value(tab : in out bitmap_array ; value : in float) is
      valuePlaced : boolean;
   begin
      valuePlaced := False;

      -- Try to add the value between imin and imax if there is some space left
      for i in tab.imin..tab.imax loop
         if not tab.bitmap(i) then
            -- Add value in the array
            tab.tab(i) := value;
            tab.bitmap(i) := True;
            valuePlaced := True;
         end if;

         exit when valuePlaced;
      end loop;

      -- If there is no space between imin and imax, try to add it before imin
      if not valuePlaced then
         if tab.imin > 1 then
            tab.imin := tab.imin-1;
            tab.tab(tab.imin) := value;
            tab.bitmap(tab.imin) := True;
            valuePlaced := True;
         end if;
      end if;

      -- If there is no space before imin, then it must be added after imax
      if not valuePlaced then
         tab.imax := tab.imax + 1;
         tab.tab(tab.imax) := value;
         tab.bitmap(tab.imax) := True;
         valuePlaced := True;
      end if;

   end add_value;

   -- Role: Searches the first occurence of the value in the array
   -- Parameters: tab - array where the value is searched
   -- value - value to seach in the array
   -- Returns: index of the first value found in the array or 0 if not found
   -- Pre:
   -- Post:
   function index_of(tab : in bitmap_array ; value : in float) return integer is
      index : integer;
   begin
      index := 0;
      for i in tab.imin..tab.imax loop
         if tab.bitmap(i) and then tab.tab(i) = value then
            index := i;
         end if;
         exit when index /= 0;
      end loop;
      return index;
   end index_of;

   -- Role: Removes the value at the specified index from the array
   -- Parameters: tab - array where the value is removed
   -- index - index of the element to remove from the array
   -- Pre: imin <= index <= imax
   -- Post: the value at the specified index is no longer in the array
   procedure remove_value_at(tab : in out bitmap_array ; index : in integer) is
      newLimit : integer;
   begin
      -- Remove value
      tab.bitmap(index) := False;
      -- Update imin if needed
      newLimit := tab.imin;
      for i in tab.imin..tab.imax loop
         if tab.bitmap(i) then
            newLimit := i;
         end if;
         exit when tab.bitmap(i);
      end loop;
      -- If the array is empty between imin and imax, then reset the imin,imax to 1
      if newLimit = tab.imin and not tab.bitmap(tab.imin) then
         tab.imin := 1;
         tab.imax := 1;
      else
         -- Else the new imin is the new limit found
         tab.imin := newLimit;
      end if;


      -- Update imax if needed
      if tab.imax /= 1 then
         newLimit := tab.imax;
         for i in reverse tab.imin..tab.imax loop
            if tab.bitmap(i) then
               newLimit := i;
            end if;
            exit when tab.bitmap(i);
         end loop;
         -- Update imax
         tab.imax := newLimit;
      end if;

   end remove_value_at;

   -- Role: Displays the array according to the specification
   -- Parameter: tab - array to display
   -- Pre:
   -- Post:
   procedure display_array(tab : in bitmap_array) is
   begin
      -- Part 1
      put("Indice min = ");
      put(tab.imin,0);
      put_line("");
      put("Indice max = ");
      put(tab.imax,0);
      put_line("");

      put_line("=====================================");
      put_line("           tableau de      tableau de");
      put_line("indices    bool�ens        valeurs   ");
      put_line("=====================================");
      for i in tab.imin..tab.imax loop
         put(i,10);
         put(" ");
         if tab.bitmap(i) then
            put("true ");
         else
            put("false");
         end if;
         put("           ");
         put(tab.tab(i),0,0,0);
         put_line("");
      end loop;
      put_line("=====================================");

      -- Part 2
      put("Nb effectif d'elements = ");
      put(nb_useful_elements(tab),0);
      put_line("");
      put_line("Valeurs");
      put_line("=======");
      for i in tab.imin..tab.imax loop
         if tab.bitmap(i) then
            put(tab.tab(i),0,0,0);
            put_line("");
         end if;
      end loop;
      put_line("=======");
   end display_array;

   -- Role: Compacts the array to remove the gaps that might exist
   -- Parameter: tab - array to compact
   -- Pre: the array is not empty
   -- Post: there is no gap in the array
   procedure compact_array(tab : in out bitmap_array) is
      currentIndex : integer;
   begin
      -- Move each meaningful value to the top of the array
      currentIndex := 1;
      for i in tab.imin..tab.imax loop
         if tab.bitmap(i) then
            tab.tab(currentIndex) := tab.tab(i);
            tab.bitmap(currentIndex) := True;
            currentIndex := currentIndex + 1;
         end if;
      end loop;
      tab.imin := 1;
      tab.imax := currentIndex - 1;
   end compact_array;



   procedure init_array(tab : out bitmap_array) is
   begin
      tab.bitmap(1) := False;
      tab.imin := 1;
      tab.imax := 1;
   end init_array;

   procedure display_instruction_title(title : string) is
   begin
      put_line("-------");
      put_line(title);
      put_line("-------");
   end display_instruction_title;




   -- Variables
   tab : bitmap_array;
begin
   -- Test program
   display_instruction_title("Initialisation");
   init_array(tab);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Ajouter 1.0");
   add_value(tab, 1.0);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Ajouter 2.0");
   add_value(tab, 2.0);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Ajouter 3.0");
   add_value(tab, 3.0);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Ajouter 4.0");
   add_value(tab, 4.0);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Ajouter 5.0");
   add_value(tab, 5.0);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Supprimer 1er");
   remove_value_at(tab, 1);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Supprimer 3eme");
   remove_value_at(tab, 3);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Supprimer 5eme");
   remove_value_at(tab, 5);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Ajouter 2.5");
   add_value(tab, 2.5);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Ajouter 1.5");
   add_value(tab, 1.5);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Ajouter 4.0");
   add_value(tab, 4.0);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Compacter");
   compact_array(tab);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Chercher 4.0");
   put(index_of(tab, 4.0),0);
   put_line("");
   put_line("");
   put_line("");

   display_instruction_title("Chercher 1.0");
   put(index_of(tab, 1.0),0);
   put_line("");
   put_line("");
   put_line("");

   display_instruction_title("Supprimer 3eme");
   remove_value_at(tab, 3);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Supprimer 4eme");
   remove_value_at(tab, 4);
   display_array(tab);
   put_line("");
   put_line("");

   display_instruction_title("Chercher 4.0");
   put(index_of(tab, 4.0),0);
   put_line("");
   put_line("");
   put_line("");

   display_instruction_title("Compacter");
   compact_array(tab);
   display_array(tab);
end Main;
