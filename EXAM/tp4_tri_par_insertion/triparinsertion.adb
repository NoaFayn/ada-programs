with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
with Ada.Text_IO;
use Ada.Text_IO;


procedure TriParInsertion is
    MAX_NOMBRES : constant Integer := 100; -- Nombre maximum d'elements du tableau
    type T_Entiers is array (1..MAX_NOMBRES) of Integer; -- Tableau d'entiers

    tab                 : T_Entiers;    -- Tableau a trier
    nbElements          : Integer;       -- Nombre total d'elements du tableau
    nbElementsTries     : Integer;       -- Nombre d'elements du tableau deja tries
    elementATrier       : Integer;       -- Element a trier
   elementTrie         : Integer;       -- Element deja trie


   -- Procedure qui affiche un tableau
   procedure afficher_tableau (tab : in T_Entiers; size : in Integer) is
   begin
      for i in 1..size loop
         Put(tab(i), 0);
      end loop;
   end afficher_tableau;

begin

    tab(1) := 9;
    tab(2) := 2;
    tab(3) := 8;
    tab(4) := 5;
    tab(5) := 1;
    tab(6) := 7;
    nbElements := 6;
   nbElementsTries := 1;

   for index in 2..nbElements loop
      elementATrier := tab(index);
      for indexTrie in 1..index loop
         elementTrie := tab(indexTrie);

         if elementATrier < elementTrie then
            tab(indexTrie) := elementATrier;
            elementATrier := elementTrie;
         end if;
      end loop;
   end loop;

end TriParInsertion;
