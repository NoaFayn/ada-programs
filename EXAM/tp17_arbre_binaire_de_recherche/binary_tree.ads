package binary_tree is
    type t_ab is private;

    no_such_element : exception;

    -- Role: Initialise a binary tree
    -- Parameter: abr - binary tree to initialise
    -- Pre:
    -- Post: abr est vide
    procedure initialise_tree (abr : out t_ab);

    -- Role: Checks if the binary tree is empty
    -- Parameter: abr - binary tree to check
    -- Returns: TRUE if the binary tree is empty and FALSE otherwise
    -- Pre: 
    -- Post: 
    function is_empty (abr : in t_ab) return boolean;

    -- Role: Computes the number of elements in the tree
    -- Parameter: abr - binary tree to compute the size on
    -- Pre:
    -- Post: 
    function size (abr : in t_ab) return integer;

    -- Role: Inserts an element in the binary tree
    -- Parameter: abr - binary tree to insert the element in
    -- data - element to insert
    -- Pre:
    -- Post: the element is in the binary tree
    procedure insert (abr : in out t_ab;
                      data : in integer);
    
    -- Role: Checks if the binary tree contains the specified element
    -- Parameters: abr - the binary tree to check into
    -- data - the element to find
    -- Returns: TRUE if the element is in the tree and FALSE otherwise
    -- Pre: 
    -- Post: 
    function contains (abr : in t_ab;
                     data : in integer) return boolean;
    
    -- Role: Changes the src element to the data element
    -- Parameters: abr - the binary tree to work on
    -- src - the element to change
    -- data - the new element to insert
    -- Pre:
    -- Post: 
    procedure edit (abr : in out t_ab;
                    src : in integer;
                    data : in integer);
    
    -- Role: Deletes the specified element from the tree
    -- Parameter: abr - the binary tree to remove the element from
    -- data - the element to remove
    -- Pre:
    -- Post:
    procedure delete (abr : in out t_ab;
                      data : in integer);
    
    -- Role: Displays the binary tree
    -- Parameter: abr - the binary tree to display
    -- Pre:
    -- Post:
    procedure display_binary_tree (abr : in t_ab);

    private
        type t_node;
        type t_ab is access t_node;
        type t_node is record
            data : integer;
            left_tree : t_ab;
            right_tree : t_ab;
        end record;
end binary_tree;