with ada.integer_text_io, ada.text_io;
use ada.integer_text_io, ada.text_io;

package body binary_tree is

    type element is record
        node : t_ab;
        left_explored : boolean;
        right_explored : boolean;
    end record;

    --
    -- Tools
    -- 

    -- Role: Inserts all of the elements found in the source tree to the dest tree
    -- Parameter: source - source tree to get the element to insert from
    -- dest - destination tree to insert the elements into
    -- Pre: the source and the dest tree are not linked
    -- Post: all of the elements from the source tree are in the dest tree
    procedure insert_recursive (source : in t_ab;
                                dest : in out t_ab) is
    begin
        if source = null then
            null;
        else
            if source.left_tree = null then
                if source.right_tree = null then
                    insert(dest, source.data);
                else
                    insert_recursive(source.right_tree, dest);
                end if;
            else
                insert_recursive(source.left_tree, dest);
            end if;
        end if;
    end insert_recursive;

    -- Role: Deletes the node with the specified data.
    -- Parameter: current - the current node that is being checked
    -- parent - the parent of the current node
    -- root - the root of the tree worked on (from where the elements will be reinserted)
    -- data - the data to delete
    -- Pre:
    -- Post: the node containing the dala element is deleted from the current and root tree and all of the nodes linked to this deleted node are reinserted in the tree
    procedure delete_recursive (current : in out t_ab;
                                parent : in out t_ab;
                                root : in out t_ab;
                                data : in integer) is
    begin
        if current = null then
            null;
        else
            if current.data = data then
                -- found node to delete
                -- cut the tree
                if parent /= null then
                    if parent.left_tree = current then
                        parent.left_tree := null;
                    else
                        parent.right_tree := null;
                    end if;
                else
                    null;
                end if;
                -- insert all of the left nodes
                insert_recursive(current.left_tree, root);
                insert_recursive(current.right_tree, root);
            else
                delete_recursive(current.left_tree, current, root, data);
                delete_recursive(current.right_tree, current, root, data);
            end if;
        end if;
    end delete_recursive;

    procedure display_binary_tree_recursive (current : in t_ab;
                                             deep : in integer) is
    begin
        if current = null then
            null;
        else
            for i in 0..deep loop
                put(" ");
            end loop;
            put(current.data,0);

            display_binary_tree_recursive(current.left_tree, deep+1);
            display_binary_tree_recursive(current.right_tree, deep+1);
        end if;
    end display_binary_tree_recursive;



    -- 
    -- Package sub programs
    --

    -- Role: Initialise a binary tree
    -- Parameter: abr - binary tree to initialise
    -- Pre:
    -- Post: abr est vide
    procedure initialise_tree (abr : out t_ab) is
    begin
        abr := null;
    end initialise_tree;

    -- Role: Checks if the binary tree is empty
    -- Parameter: abr - binary tree to check
    -- Returns: TRUE if the binary tree is empty and FALSE otherwise
    -- Pre: 
    -- Post: 
    function is_empty (abr : in t_ab) return boolean is
    begin
        return abr = null;
    end is_empty;

    -- Role: Computes the number of elements in the tree
    -- Parameter: abr - binary tree to compute the size on
    -- Pre:
    -- Post: 
    function size (abr : in t_ab) return integer is
        count : integer;
    begin
        -- Empty tree
        if is_empty(abr) then
            count := 0;
        else
            -- Count myself
            count := 1;
            -- Count left
            count := count + size(abr.left_tree);
            -- Count right
            count := count + size(abr.right_tree);
        end if;

        return count;
    end size;

    -- Role: Inserts an element in the binary tree
    -- Parameter: abr - binary tree to insert the element in
    -- data - element to insert
    -- Pre:
    -- Post: the element is in the binary tree
    procedure insert (abr : in out t_ab;
                      data : in integer) is
    begin
        if is_empty(abr) then
            abr := new t_node'(data => data, right_tree => null, left_tree => null);
        else
            if data <= abr.data then
                if abr.left_tree = null then
                    abr.left_tree := new t_node'(data => data, right_tree => null, left_tree => null);
                else
                    insert(abr.left_tree, data);
                end if;
            else
                if abr.right_tree = null then
                    abr.right_tree := new t_node'(data => data, right_tree => null, left_tree => null);
                else
                    insert(abr.right_tree, data);
                end if;
            end if;
        end if;
    end insert;
    
    -- Role: Checks if the binary tree contains the specified element
    -- Parameters: abr - the binary tree to check into
    -- data - the element to find
    -- Returns: TRUE if the element is in the tree and FALSE otherwise
    -- Pre: 
    -- Post: 
    function contains (abr : in t_ab;
                     data : in integer) return boolean is
        has_data : boolean;
    begin
        has_data := false;
        if is_empty(abr) then
            null;
        else
            if data = abr.data then
                has_data := true;
            else
                if data < abr.data then
                    has_data := contains(abr.left_tree, data);
                else
                    has_data := contains(abr.right_tree, data);
                end if;
            end if;
        end if;

        return has_data;
    end contains;
    
    -- Role: Changes the src element to the data element
    -- Parameters: abr - the binary tree to work on
    -- src - the element to change
    -- data - the new element to insert
    -- Pre:
    -- Post: 
    procedure edit (abr : in out t_ab;
                    src : in integer;
                    data : in integer) is
    begin
    null;
    end edit;
    
    -- Role: Deletes the specified element from the tree
    -- Parameter: abr - the binary tree to remove the element from
    -- data - the element to remove
    -- Pre:
    -- Post:
    -- Exception: no_such_element - if the element is not in the tree
    procedure delete (abr : in out t_ab;
                      data : in integer) is
        parent : t_ab;
        current : t_ab;
    begin
        parent := null;
        delete_recursive (abr, parent, abr, data);
    end delete;

    
    -- Role: Displays the binary tree
    -- Parameter: abr - the binary tree to display
    -- Pre:
    -- Post:
    procedure display_binary_tree (abr : in t_ab) is
        to_display : list;
    begin
        display_binary_tree_recursive(abr, 0);
    end display_binary_tree;
end binary_tree;