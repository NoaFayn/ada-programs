
with ada.Integer_Text_IO, ada.Text_IO;
use ada.Integer_Text_IO, ada.Text_IO;

procedure Main is

   -- Linked list
   type linked_element;
   type linked_list is access linked_element;
   type linked_element is record
      next : linked_list;
      value : integer;
   end record;

   -- Exceptions
   element_not_in_list : exception; -- raised when the element is not in the list
   list_empty : exception; -- raised when the list is empty

   -- Role: Creates an empty list
   -- Returns: linked list
   -- Post: linked list is empty
   function create_empty_list return linked_list is
   begin
      return null;
   end create_empty_list;

   -- Role: Checks if a list is empty
   -- Parameter: l - list
   -- Returns: TRUE if the list is empty and FALSE otherwise
   function is_empty(l : in linked_list) return boolean is
   begin
      return l = null;
   end is_empty;

   -- Role: Inserts the new element as the first element in the list
   -- Parameters: l - list to add the element to
   -- new_element - new element to add in the list
   -- Post: new_element belongs to the list
   procedure insert_first_element(l : in out linked_list;
                                  new_element : in integer) is
   begin
      if l = null then
         l := new linked_element'(next => null, value => new_element);
      else
         l := new linked_element'(next => l, value => new_element);
      end if;
   end insert_first_element;

   -- Role: Displays the elements of the list
   -- Parameter: l - list to display
   procedure display_list(l : in linked_list) is
   begin
      if l = null then
         new_line;
      else
         put(l.value,0);put(" ");
         display_list(l.next);
      end if;
   end display_list;

   -- Role: Searches if the element e is in the list l
   -- Parameters: l - list to search the element in
   -- e - element to search
   -- Returns: address of the element or null if the list is empty or if e is not in the list
   function search(l : in linked_list;
                   e : in integer)
                   return linked_list
   is
      current_list_element : linked_list;
      found_element : linked_list;
   begin
      if is_empty(l) then
         return null;
      end if;

      found_element := null;
      current_list_element := l;
      while current_list_element /= null and found_element = null loop
         if current_list_element.value = e then
            found_element := current_list_element;
         else
            null;
         end if;
         current_list_element := current_list_element.next;
      end loop;

      return found_element;
   end search;

   -- Role: Inserts in the list l the new element after the data value
   -- Parameters: l - the list to add the element in
   -- new_element - the new element to add in the list
   -- data - value to add the new element after
   -- Post: new_element is in the list
   -- Exceptions: element_not_in_list - if the data value is not in the list
   -- list_empty - if the list is empty
   procedure insert_after(l : in linked_list;
                          new_element : in integer;
                          data : in integer) is
      element_found : linked_list;
   begin
      if is_empty(l) then
         raise list_empty;
      end if;

      element_found := search(l,data);
      if element_found = null then
         raise element_not_in_list;
      end if;

      -- Insert new element
      element_found.next := new linked_element'(next => element_found.next, value => new_element);
   end insert_after;

   -- Role: Inserts in the list l the new element before the data value
   -- Parameters: l - list to insert the new element in
   -- new_element - the new element to insert
   -- data - value to add the new element before
   -- Post: new_element is in the list
   -- Exceptions: element_not_in_list - if the data value is not in the list
   -- list_empty - if the list is empty
   procedure insert_before(l : in out linked_list;
                           new_element : in integer;
                           data : in integer) is
      current_list_element : linked_list;
      added : boolean;
   begin
      if is_empty(l) then
         raise list_empty;
      end if;

      if l.value = data then
         l := new linked_element'(next => l, value => new_element);
         added := true;
      else
         current_list_element := l;
         added := false;
         while current_list_element.next /= null and not added loop
            if current_list_element.next.value = data then
               current_list_element.next := new linked_element'(next => current_list_element.next, value => new_element);
               added := true;
            end if;
            current_list_element := current_list_element.next;
         end loop;

         if not added then
            raise element_not_in_list;
         end if;
      end if;

   end insert_before;

   -- Role: Removes the element e from the list l
   -- Parameters: l - list to remove the element from
   -- e - element to remove from the list
   -- Post: e is not in the list
   procedure remove(l : in out linked_list;
                    e : in integer) is
      previous_element : linked_list;
      current_element : linked_list;
      removed : boolean;
   begin
      if l.value = e then
         l := l.next;
      else
         previous_element := l;
         current_element := l.next;
         removed := false;

         while current_element /= null and not removed loop
            if current_element.value = e then
               previous_element.next := current_element.next;
               removed := true;
            end if;
            previous_element := current_element;
            current_element := current_element.next;
         end loop;
      end if;
   end remove;



   -- Variables
   list : linked_list;
   element_found : linked_list;
begin

   list := create_empty_list;

   if is_empty(list) then
      put_line("List is empty");
   else
      put_line("List is NOT empty");
   end if;

   put_line("Inserting first element 10");
   insert_first_element(list, 10);
   display_list(list);

   put_line("Inserting first element 100");
   insert_first_element(list, 100);
   display_list(list);

   put_line("Searching element 10");
   element_found := search(list, 10);
   if element_found /= null then
      put_line("Found!");
   else
      put_line("NOT found.");
   end if;

   put_line("Inserting 1 after 100");
   insert_after(list, 1, 100);
   display_list(list);

   put_line("Inserting 2 after 10");
   insert_after(list, 2, 10);
   display_list(list);

   put_line("Inserting 5 before 2");
   insert_before(list, 5, 2);
   display_list(list);

   put_line("Inserting 6 before 1");
   insert_before(list, 6, 1);
   display_list(list);

   put_line("Inserting 7 before 100");
   insert_before(list, 7, 100);
   display_list(list);

   put_line("Removing element 1 to 10");
   for i in 1..10 loop
      remove(list, i);
      display_list(list);
   end loop;

   -- Exceptions
   put_line("Trying to raise element_not_in_list");
   begin
      insert_after(list, 1, 1);
   exception
      when element_not_in_list => put_line("Raised!");
   end;
   begin
      insert_before(list, 1, 1);
   exception
      when element_not_in_list => put_line("Raised!");
   end;

   put_line("Trying to raise list_empty");
   remove(list, 100);
   begin
      insert_after(list, 1, 1);
   exception
      when list_empty => put_line("Raised!");
   end;
   begin
      insert_before(list, 1, 1);
   exception
      when list_empty => put_line("Raised!");
   end;


end Main;
