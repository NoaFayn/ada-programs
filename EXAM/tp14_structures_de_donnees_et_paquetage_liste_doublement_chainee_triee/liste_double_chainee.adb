with ada.integer_text_io, ada.text_io;
use ada.integer_text_io, ada.text_io;

package body liste_double_chainee is
    -- Role: Creates an empty list
    -- Parameter: l - list that will be initialised
    -- Pre:
    -- Post: the list is empty
    procedure create_list (l : out list) is
    begin
        l := null;
    end create_list;

    -- Role: Adds an element in the list
    -- Parameters: l - list to add the element in
    -- e - element to add in the list
    -- Pre:
    -- Post: the current element of the list is the element added and the list is ordered
    procedure add_element (l : in out list;
                           e : in integer) is
        tmp : list;
    begin
        if l = null then
            l := new list_element'(previous => null,
                                  next => null,
                                  value => e);
        else
            if l.value <= e then
                if l.next = null then
                    -- Insert at the end of the list
                    tmp := new list_element'(previous => l,
                                              next => null,
                                              value => e);
                    l.next := tmp;
                    l := tmp;
                else
                    if l.next.value >= e then
                        -- Insert in the middle
                        tmp := new list_element'(previous => l,
                                                  next => l.next,
                                                  value => e);
                        l.next.previous := tmp;
                        l.next := tmp;
                        l := tmp;
                    else
                        -- Pass to the next
                        add_element(l.next, e);
                    end if;
                end if;
            else
                if l.previous = null then
                    -- Insert at the beginning of the list
                    tmp := new list_element'(previous => null,
                                           next => l,
                                           value => e);
                    l.previous := tmp;
                    l := tmp;
                else
                    if l.previous.value <= e then
                        -- Insert in the middle
                        tmp := new list_element'(previous => l.previous,
                                               next => l,
                                               value => e);
                        l.previous.next := tmp;
                        l.previous := tmp;
                        l := tmp;
                    else
                        -- Pass to the previous
                        add_element(l.previous, e);
                    end if;
                end if;
            end if;
        end if;
    end add_element;

    -- Role: Removes the specified element from the list
    -- Parameters: l - list to remove the element from
    -- e - element to remove
    -- Pre:
    -- Post: the element is removed from the list and the list is ordered
    -- Exception: no_such_element - if the specified element is not in the list
    procedure remove_element (l : in out list;
                              e : in integer) is
        tmp : list;
    begin
        if l = null then
            raise no_such_element;
        end if;

        if l.value = e then
            --put_line("Found !");
            -- Remove current
            if l.previous = null then
                --put_line("l.previous null");
                if l.next = null then
                    --put_line("l.next null");
                    l := null;
                else
                    --put_line("l.next NOT null");
                    l.next.previous := null;
                    l := l.next;
                end if;
            else
                --put_line("l.previous NOT null");
                if l.next = null then
                    --put_line("l.next null");
                    l.previous.next := null;
                    l := l.previous;
                else
                    --put_line("l.next NOT null");
                    --put("ok");
                    l.previous.next := l.next;
                    l.next.previous := l.previous;
                    l := l.previous;
                    --put_line("After modification:");
                    --put("previous: ");put(l.previous.value,0);new_line;
                    --put("previous.next: ");put(l.previous.next.value,0);new_line;
                    --put("current: ");put(l.value,0);new_line;
                    --put("next.previous: ");put(l.next.previous.value,0);new_line;
                    --put("next: ");put(l.next.value,0);new_line;
                end if;
            end if;
        else
            if l.value > e then
                --put("not found yet: ");put(l.value,0);put(">");put(e,0);new_line;
                tmp := l.previous;
                remove_element(tmp, e);
                l := tmp;
            else
                --put("not found yet: ");put(l.value,0);put("<");put(e,0);new_line;
                tmp := l.next;
                remove_element(tmp, e);
                l := tmp;
            end if;
        end if;
    end remove_element;

    -- Role: Searches the specified element in the list
    -- Parameters: l - list to search the element in
    -- e - element to search
    -- Pre: 
    -- Post: the current element of the list is the element found or left unchanged if no element was found
    procedure search_element (l : in out list;
                              e : in integer) is
        tmp : list;
        found : boolean;
    begin
        tmp := l;
        found := false;
        while not found and tmp /= null loop
            if tmp.value = e then
                found := true;
            else
                if tmp.value > e then
                    tmp := tmp.previous;
                else
                    tmp := tmp.next;
                end if;
            end if;
        end loop;
        if found then
            l := tmp;
        end if;
    end search_element;
    
    -- Role: Displays all of the elements of the list, from the first to the last
    -- Parameter: l - list to display the element of
    -- Pre:
    -- Post:
    procedure display_list (l : in list) is
        current_element : list;
    begin
        if l = null then
            -- Do nothing
            null;
        else
            -- Go to the first element
            current_element := l;
            while current_element.previous /= null loop
                current_element := current_element.previous;
            end loop;

            -- Display every element
            while current_element /= null loop
                put(current_element.value, 0);
                put(" ");
                current_element := current_element.next;
            end loop;

            -- Display current
            new_line;
            put("(Current is ");
            put(l.value, 0);
            put(")");
            new_line;
        end if;
    end display_list;

end liste_double_chainee;