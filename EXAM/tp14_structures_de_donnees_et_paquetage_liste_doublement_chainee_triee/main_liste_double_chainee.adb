with ada.integer_text_io, ada.text_io;
use ada.integer_text_io, ada.text_io;

with liste_double_chainee;
use liste_double_chainee;

procedure main_liste_double_chainee is
    l : list;
begin
    put_line("Create list");
    create_list(l);

    put_line("Add 3");
    add_element(l, 3);
    display_list(l);

    put_line("Add 6");
    add_element(l, 6);
    display_list(l);

    put_line("Add 7");
    add_element(l, 7);
    display_list(l);

    put_line("Add 9");
    add_element(l, 9);
    display_list(l);

    put_line("Search 6");
    search_element(l, 6);
    display_list(l);

    put_line("Remove 7");
    remove_element(l, 7);
    display_list(l);

    put_line("Remove 9");
    remove_element(l, 9);
    display_list(l);

    put_line("Remove 3");
    remove_element(l, 3);
    display_list(l);

    put_line("Remove 6");
    remove_element(l, 6);
    display_list(l);

    put_line("Add 10");
    add_element(l, 10);
    display_list(l);

    put_line("Raise no_such_element");
    begin
        remove_element(l, 3);
    exception
        when no_such_element => put_line("OK!");
    end;
end main_liste_double_chainee;