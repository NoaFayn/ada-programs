
generic
    type g_value is private;
    with function "=" (X,Y : g_value) return boolean;
    with function image (X : g_value) return string;

package double_linked_list_gen is
    type double_linked_list is private;

    no_such_element : exception;

    -- Role: Creates an empty list
    -- Parameter: l - list that will be initialised
    -- Pre:
    -- Post: the list is empty
    procedure create_list (l : out double_linked_list);

    -- Role: Checks if the list is empty
    -- Parameter: l - the list to check
    -- Pre:
    -- Post:
    function is_empty (l : in double_linked_list) return boolean;

    -- Role: Adds an element in the list
    -- Parameters: l - list to add the element in
    -- e - element to add in the list
    -- Pre:
    -- Post: the current element of the list is the element added
    procedure add_element (l : in out double_linked_list;
                           e : in g_value);
    
    -- Role: Removes the specified element from the list
    -- Parameters: l - list to remove the element from
    -- e - element to remove
    -- Pre:
    -- Post: the element is removed from the list
    -- Exception: no_such_element - if the specified element is not in the list
    procedure remove_element (l : in out double_linked_list;
                              e : in g_value);

    -- Role: Checks if the specified element is in the list
    -- Parameters: l - list to search the element in
    -- e - element to search
    -- Returns: TRUE if at least one occurence of the element e is in the list and FALSE otherwise
    -- Pre: 
    -- Post:
    function contains (l : in out double_linked_list;
                              e : in g_value) return boolean;
    
    -- Role: Displays all of the elements of the list, from the first to the last
    -- Parameter: l - list to display the element of
    -- Pre:
    -- Post:
    procedure display_list (l : in double_linked_list);

    private
        type list_element;
        type double_linked_list is access list_element;
        type list_element is record
            previous : double_linked_list;
            next : double_linked_list;
            value : g_value;
        end record;

end double_linked_list_gen;