with pile_gen;

package p_calculatrice_4 is

    package pile_integer is new pile_gen (g_value => integer,
                                          image => integer'image);
    use pile_integer;

    type calculator is record
        calc : stack;
        mem : integer;
    end record;
    
    zero_division : exception;
    not_enough_operands : exception;

    -- Role: Initialise the calculator
    -- Parameter: calc - the calculator to initialise
    -- Pre:
    -- Post: calc is initialised and empty
    procedure initialise_calculator (calc : out calculator);

    -- Role: Reset the calculator
    -- Parameter: calc - the calculator to reset
    -- Pre:
    -- Post: calc is reset and empty
    procedure reset_calculator (calc : out calculator);

    -- Role: Write a value in the calculator
    -- Parameter: calc - the calculator to write in
    -- e - the value to write
    -- Pre:
    -- Post: the value is written in the calculator
    procedure write_value (calc : in out calculator;
                           e : in integer);

    -- Role: Performs an addition of the two values written in the calculator and place the result as a new value in the calculator
    -- Parameter: calc - the calculator to perform the operation on
    -- Pre:
    -- Post: the two value written are added and the result replace those value
    procedure op_add (calc : in out calculator);

    -- Role: Performs a substraction of the two values written in the calculator and place the result as a new value in the calculator
    -- Parameter: calc - the calculator to perform the operation on
    -- Pre:
    -- Post: the two value written are subtracted and the result replace those value
    procedure op_sub (calc : in out calculator);

    -- Role: Performs a multiplication of the two values written in the calculator and place the result as a new value in the calculator
    -- Parameter: calc - the calculator to perform the operation on
    -- Pre:
    -- Post: the two value written are multiplied and the result replace those value
    procedure op_mult (calc : in out calculator);

    -- Role: Performs a division of the two values written in the calculator and place the result as a new value in the calculator
    -- Parameter: calc - the calculator to perform the operation on
    -- Pre:
    -- Post: the two value written are divided and the result replace those value
    procedure op_div (calc : in out calculator);

    -- Role: Displays the calculator
    -- Parameter: calc - the calculator to display
    -- Pre:
    -- Post:
    procedure display_calculator (calc : in calculator);

    -- Role: Clears the memory of any previous value
    -- Parameter: calc - the calculator to clear the memory of
    -- Pre:
    -- Post: the memory is set to 0
    procedure mem_clear (calc : in out calculator);

    -- Role: Adds the current value of the calculator in the memory
    -- Parameter: calc - the calculator to increment the value on
    -- Pre:
    -- Post: the value of the memory is added to the current value of the calculator
    procedure mem_inc (calc : in out calculator);

    -- Role: Substracts the value of the memory with the current value of the calculator
    -- Parameter: calc - the calculator to decrement the value on
    -- Pre:
    -- Post: the current value of the calculator is substracted to the value of the memory
    procedure mem_dec (calc : in out calculator);

    -- Role: Places the current value of the memory in the current value of the calculator
    -- Parameter: calc - the calculator to act on
    -- Pre:
    -- Post: the memorised value is set as the current value in the calculator
    procedure mem_read (calc : in out calculator);


end p_calculatrice_4;