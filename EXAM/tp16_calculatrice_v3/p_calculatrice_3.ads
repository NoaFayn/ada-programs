with pile_gen;

package p_calculatrice_3 is
    package pile_integer is new pile_gen (g_value => integer,
                                          image => integer'image);
    use pile_integer;

    type calculator is record
        calc : stack;
    end record;

    -- Role: Initialise the calculator
    -- Parameter: calc - the calculator to initialise
    -- Pre:
    -- Post: calc is initialised and empty
    procedure initialise_calculator (calc : out calculator);

    -- Role: Reset the calculator
    -- Parameter: calc - the calculator to reset
    -- Pre:
    -- Post: calc is reset and empty
    procedure reset_calculator (calc : out calculator);

    -- Role: Write a value in the calculator
    -- Parameter: calc - the calculator to write in
    -- e - the value to write
    -- Pre:
    -- Post: the value is written in the calculator
    procedure write_value (calc : in out calculator;
                           e : in integer);

    -- Role: Performs an addition of the two values written in the calculator and place the result as a new value in the calculator
    -- Parameter: calc - the calculator to perform the operation on
    -- Pre:
    -- Post: the two value written are added and the result replace those value
    procedure op_add (calc : in out calculator);

    -- Role: Performs a substraction of the two values written in the calculator and place the result as a new value in the calculator
    -- Parameter: calc - the calculator to perform the operation on
    -- Pre:
    -- Post: the two value written are subtracted and the result replace those value
    procedure op_sub (calc : in out calculator);

    -- Role: Performs a multiplication of the two values written in the calculator and place the result as a new value in the calculator
    -- Parameter: calc - the calculator to perform the operation on
    -- Pre:
    -- Post: the two value written are multiplied and the result replace those value
    procedure op_mult (calc : in out calculator);

    -- Role: Performs a division of the two values written in the calculator and place the result as a new value in the calculator
    -- Parameter: calc - the calculator to perform the operation on
    -- Pre:
    -- Post: the two value written are divided and the result replace those value
    procedure op_div (calc : in out calculator);

    -- Role: Displays the calculator
    -- Parameter: calc - the calculator to display
    -- Pre:
    -- Post:
    procedure display_calculator (calc : in calculator);


end p_calculatrice_3;
