with ada.integer_text_io, ada.text_io;
use ada.integer_text_io, ada.text_io;

with p_calculatrice_4;
use p_calculatrice_4;

procedure main_calculatrice4 is

    -- Variables
   calc : calculator; -- calculator for the operations

   command : string(1..10); -- can be an integer or an operation (+,-,*,/)
   length : integer; -- number of character of the command

   n : integer; -- integer from the command

   stop : boolean; -- TRUE when the program has to stop and FALSE otherwise
begin
    initialise_calculator(calc);
    stop := false;
   while not stop loop
      -- Display the calc
      display_calculator(calc);
      -- Get the command
      put_line("Please enter an integer or an operator or a memory operation");
      put("> ");
      get_line(command, length);
      -- Check if the command is a number
      begin
         -- Get the integer
         n := integer'value(command(1..length));
         -- Push the integer in the stack
         write_value(calc, n);
      exception
         when Constraint_Error =>
            begin
                -- Not an integer
                if command(1..4) = "stop" then
                -- Stop the program
                stop := true;
                elsif command(1..5) = "reset" or command(1..5) = "clear" then
                -- Reset the stack
                reset_calculator(calc);
                
                -- Memory operations
                elsif command(1..2) = "mc" or command(1..2) = "MC" then
                    -- Memory clear
                    mem_clear(calc);
                elsif command(1..2) = "m+" or command(1..2) = "M+" then
                    -- Memory inc
                    mem_inc(calc);
                elsif command(1..2) = "m-" or command(1..2) = "M-" then
                    -- Memory dec
                    mem_dec(calc);
                elsif command(1..2) = "mr" or command(1..2) = "MR" then
                    -- Memory read
                    mem_read(calc);

                -- Do the operation
                elsif command(1..1) = "+" then
                    -- Add
                    op_add(calc);
                elsif command(1..1) = "-" then
                    -- Sub
                    op_sub(calc);
                elsif command(1..1) = "*" then
                    -- Mult
                    op_mult(calc);
                elsif command(1..1) = "/" then
                    -- Div
                    op_div(calc);
                else
                    -- Not a valid operation
                    put_line("Not a valid operator");
                end if;
            exception
                when not_enough_operands =>
                    put_line("Not enough operands");
                when zero_division =>
                    put_line("Cannot divide by 0");
            end;
      end;
   end loop;

end main_calculatrice4;