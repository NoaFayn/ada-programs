with ada.Integer_Text_IO, ada.Text_IO;
use ada.Integer_Text_IO, ada.Text_IO;

with pile;
use pile;

procedure Main is

   s : stack; -- stack for the operations

   command : string(1..10); -- can be an integer or an operation (+,-,*,/)
   length : integer; -- number of character of the command

   n : integer; -- integer from the command

   a : integer; -- first operand
   b : integer; -- second operand
   result : integer; -- result of the operation
   doOperation : boolean; -- TRUE if the operation can be done and FALSE otherwise
   validOperator : boolean; -- TRUE if the operator is valid and FALSE otherwise

   stop : boolean; -- TRUE when the program has to stop and FALSE otherwise

begin
   stop := false;
   while not stop loop
      -- Display the stack
      display_stack(s);
      -- Get the command
      put_line("Please enter an integer or an operator");
      put("> ");
      get_line(command, length);
      -- Check if the command is a number
      begin
         -- Get the integer
         n := integer'value(command(1..length));
         -- Push the integer in the stack
         push(s, n);
      exception
         when Constraint_Error =>
            -- Not an integer
            if command(1..4) = "stop" then
               -- Stop the program
               stop := true;
            elsif command(1..5) = "reset" or command(1..5) = "clear" then
               -- Reset the stack
               create_stack(s);
            else
               -- Do the operation
               doOperation := true;
               -- Get the 2 operands
               begin
                  a := pop(s);
                  begin
                     b := pop(s);
                  exception
                     when empty_stack =>
                        doOperation := false;
                        -- Restore a
                        push(s,a);
                  end;
               exception
                  when empty_stack =>
                     doOperation := false;
               end;

               if doOperation then
                  -- Do the operation
                  validOperator := true;
                  if command(1..1) = "+" then
                     -- Add
                     result := a+b;
                  elsif command(1..1) = "-" then
                     -- Sub
                     result := a-b;
                  elsif command(1..1) = "*" then
                     -- Mult
                     result := a*b;
                  elsif command(1..1) = "/" then
                     -- Div
                     result := a/b;
                  else
                     -- Not a valid operation
                     put_line("Not a valid operator");
                     validOperator := false;
                  end if;

                  if validOperator then
                     -- Push the result
                     push(s, result);
                  else
                     -- Push back a and b
                     push(s,b);
                     push(s,a);
                  end if;
               else
                 put_line("Not enough operands in the stack");
               end if;
            end if;
      end;
   end loop;


end Main;
