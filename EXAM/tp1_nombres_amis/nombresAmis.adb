with Ada.Float_Text_IO; use Ada.Float_Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Text_IO; use Ada.Text_IO;

--------------------------------
-- nom : nombresAmis
-- séantique : Affiche tous les couples (N,M) de nombres amis tels que 0<N<=MAX, MAX entier lu au clavier.
-- tests :
-- MAX = 284 -> (220,284)
-- MAX = 1210 -> (220,284) (1184,1210)
-- MAX = 1 -> redemander MAX
-- MAX = 100001 -> redemander MAX
--------------------------------

procedure nombresAmis is
  MAX : Integer; -- entier lu
  M : Integer; -- somme des diviseurs de N
  S : Integer; 
begin

  -- R0 : Afficher tous les couples de nombres amis
  -- R1 : Saisir MAX
  loop
    Put_Line("Entrer MAX");
    MAX := Integer'Value(Get_Line);
    exit when MAX >= 1;
  end loop;

  -- R1 : Afficher les couples de nombres amis
  for N in 1..MAX loop
    -- R2 : Sommer les diviseurs de N dans M sans N
    M := 0;
    for i in 1..N/2 loop
      -- R3 : si i divise N
      if (N mod i = 0) then
        M := M + i;
      else
        NULL;
      end if;
    end loop;

    if M < N then
      -- R2 : Sommer les diviseurs de M dans S sans M
      S := 0;
      for i in 1..M/2 loop
        if (M mod i = 0) then
          S := S + i;
        else
          NULL;
        end if;
      end loop;

      if S = N then
        -- R3 : Afficher la paire (M,N) amis
        Put("("); Put(M,0); Put(";"); Put(N,0); Put(") ");
      else
        NULL;
      end if;
    end if;
  end loop;

end nombresAmis;
