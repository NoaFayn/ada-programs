package body p_calculatrice_4 is

    -- Role: Initialise the calculator
    -- Parameter: calc - the calculator to initialise
    -- Pre:
    -- Post: calc is initialised and empty
    procedure initialise_calculator (calc : out calculator) is
    begin
        create_stack(calc.calc);
        calc.mem := 0;
    end initialise_calculator;

    -- Role: Reset the calculator
    -- Parameter: calc - the calculator to reset
    -- Pre:
    -- Post: calc is reset and empty
    procedure reset_calculator (calc : out calculator) is
    begin
        create_stack(calc.calc);
    end reset_calculator;

    -- Role: Write a value in the calculator
    -- Parameter: calc - the calculator to write in
    -- e - the value to write
    -- Pre:
    -- Post: the value is written in the calculator
    procedure write_value (calc : in out calculator;
                           e : in integer) is
    begin
        push(calc.calc, e);
    end write_value;

    -- Role: Performs an addition of the two values written in the calculator and place the result as a new value in the calculator
    -- Parameter: calc - the calculator to perform the operation on
    -- Pre:
    -- Post: the two value written are added and the result replace those value
    -- Exception: not_enough_operands - if there is not enough operands
    procedure op_add (calc : in out calculator) is
        a : integer; -- first value
        b : integer; -- second value
    begin
        begin
            a := pop(calc.calc);
            begin
                b := pop(calc.calc);
                push(calc.calc, a+b);
            exception
                when empty_stack =>
                    push(calc.calc, a);
                    raise not_enough_operands;
            end;
        exception
            when empty_stack => raise not_enough_operands;
        end;
    end op_add;

    -- Role: Performs a substraction of the two values written in the calculator and place the result as a new value in the calculator
    -- Parameter: calc - the calculator to perform the operation on
    -- Pre:
    -- Post: the two value written are subtracted and the result replace those value
    procedure op_sub (calc : in out calculator) is
        a : integer; -- first value
        b : integer; -- second value
    begin
        begin
            a := pop(calc.calc);
            begin
                b := pop(calc.calc);
                push(calc.calc, a-b);
            exception
                when empty_stack =>
                    push(calc.calc, a);
                    raise not_enough_operands;
            end;
        exception
            when empty_stack => raise not_enough_operands;
        end;
    end op_sub;

    -- Role: Performs a multiplication of the two values written in the calculator and place the result as a new value in the calculator
    -- Parameter: calc - the calculator to perform the operation on
    -- Pre:
    -- Post: the two value written are multiplied and the result replace those value
    procedure op_mult (calc : in out calculator) is
        a : integer; -- first value
        b : integer; -- second value
    begin
        begin
            a := pop(calc.calc);
            begin
                b := pop(calc.calc);
                push(calc.calc, a*b);
            exception
                when empty_stack =>
                    push(calc.calc, a);
                    raise not_enough_operands;
            end;
        exception
            when empty_stack => raise not_enough_operands;
        end;
    end op_mult;

    -- Role: Performs a division of the two values written in the calculator and place the result as a new value in the calculator
    -- Parameter: calc - the calculator to perform the operation on
    -- Pre:
    -- Post: the two value written are divided and the result replace those value
    -- Exception: zero_division - if the second value is a 0
    procedure op_div (calc : in out calculator) is
        a : integer; -- first value
        b : integer; -- second value
    begin
        begin
            a := pop(calc.calc);
            begin
                b := pop(calc.calc);
                begin
                    push(calc.calc, a/b);
                exception
                    when constraint_error => raise zero_division;
                end;
            exception
                when empty_stack =>
                    push(calc.calc, a);
                    raise not_enough_operands;
            end;
        exception
            when empty_stack => raise not_enough_operands;
        end;
    end op_div;

    -- Role: Displays the calculator
    -- Parameter: calc - the calculator to display
    -- Pre:
    -- Post:
    procedure display_calculator (calc : in calculator) is
    begin
        display_stack(calc.calc);
    end display_calculator;

    -- Role: Clears the memory of any previous value
    -- Parameter: calc - the calculator to clear the memory of
    -- Pre:
    -- Post: the memory is set to 0
    procedure mem_clear (calc : in out calculator) is
    begin
        calc.mem := 0;
    end mem_clear;

    -- Role: Adds the current value of the calculator in the memory
    -- Parameter: calc - the calculator to increment the value on
    -- Pre:
    -- Post: the value of the memory is added to the current value of the calculator
    procedure mem_inc (calc : in out calculator) is
    begin
        calc.mem := calc.mem + peek(calc.calc);
    end mem_inc;

    -- Role: Substracts the value of the memory with the current value of the calculator
    -- Parameter: calc - the calculator to decrement the value on
    -- Pre:
    -- Post: the current value of the calculator is substracted to the value of the memory
    procedure mem_dec (calc : in out calculator) is
    begin
        calc.mem := calc.mem - peek(calc.calc);
    end mem_dec;

    -- Role: Places the current value of the memory in the current value of the calculator
    -- Parameter: calc - the calculator to act on
    -- Pre:
    -- Post: the memorised value is set as the current value in the calculator
    procedure mem_read (calc : in out calculator) is
    begin
        push(calc.calc, calc.mem);
    end mem_read;


end p_calculatrice_4;