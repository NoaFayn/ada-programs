
with ada.integer_text_io, ada.text_io;
use ada.integer_text_io, ada.text_io;

with binary_tree;
use binary_tree;

procedure test_binary_tree is
    
    assert_error : exception;

    procedure check (b : in boolean; msg : in string) is
    begin
        if b then
            put(".");
        else
            put("ERROR> ");
            put_line(msg);
            raise assert_error;
        end if;
    end check;

    -- Variables
    tree : t_ab;

begin

    initialise_tree(tree);
    check(is_empty(tree),"Not empty");

end test_binary_tree;