
with ada.Integer_Text_IO, ada.Text_IO;
use ada.Integer_Text_IO, ada.Text_IO;

package body pile_gen is

   -- Role: Creates an empty stack
   -- Parameter: s - the stack to initialise
   -- Pre:
   -- Post: s is empty
   procedure create_stack (s : out stack) is
   begin
      s := null;
   end create_stack;
   
   
   -- Role: Checks if the stack is empty
   -- Parameter: s - the stack to check
   -- Returns: TRUE if the stack is empty and FALSE otherwise
   -- Pre:
   -- Post:
   function is_empty (s : in stack) return boolean is
   begin
      return s = null;
   end is_empty;
   
   
   -- Role: Pushes an element at the top of the stack
   -- Parameters: s - the stack to push the element into
   -- e - the element to push
   -- Pre:
   -- Post: the element is at the top of the stack
   procedure push (s : in out stack;
                   e : in g_value) is
   begin
      s := new element'(value => e, next => s);
   end push;
   
   
   -- Role: Pops the element at the top of the stack
   -- Parameter: s - the stack to pop the element from
   -- Returns: the first element of the stack
   -- Pre:
   -- Post: the first element of the stack is removed
   -- Exception: empty_stack - if the stack is empty
   function pop (s : in out stack) return g_value is
      value : g_value;
   begin
      if is_empty(s) then
         raise empty_stack;
      end if;
      
      value := s.value;
      s := s.next;
      return value;
   end pop;

   -- Role: Peeks at the top element of the stack without poping it
   -- Parameter: s - the stack to peek the element on
   -- Returns: the first element of the stack
   -- Pre:
   -- Post:
   -- Exception: empty_stack - if the stack is empty
   function peek (s : in stack) return g_value is
   begin
      return s.value;
   end peek;
   
   -- Role: Displays the stack as follow:
   -- |   |
   -- |  X|
   -- |  X|
   -- -----
   -- Parameter: s - the stack to display
   -- Pre:
   -- Post:
   procedure display_stack (s : in stack) is
      current : stack;
   begin      
      put_line("|   |");
      current := s;
      while current /= null loop
         put("|");put(image(current.value));put("|");new_line;
         current := current.next;
      end loop;
      put_line("-----");
   end display_stack;
   

end pile_gen;
