package pile is

   type element;
   type stack is access element;
   type element is record
      value : integer;
      next : stack;
   end record;
   
   empty_stack : exception;
   
   -- Role: Creates an empty stack
   -- Parameter: s - the stack to initialise
   -- Pre:
   -- Post: s is empty
   procedure create_stack (s : out stack);
   
   -- Role: Checks if the stack is empty
   -- Parameter: s - the stack to check
   -- Returns: TRUE if the stack is empty and FALSE otherwise
   -- Pre:
   -- Post:
   function is_empty (s : in stack) return boolean;
   
   -- Role: Pushes an element at the top of the stack
   -- Parameters: s - the stack to push the element into
   -- e - the element to push
   -- Pre:
   -- Post: the element is at the top of the stack
   procedure push (s : in out stack;
                   e : in integer);
   
   -- Role: Pops the element at the top of the stack
   -- Parameter: s - the stack to pop the element from
   -- Returns: the first element of the stack
   -- Pre:
   -- Post: the first element of the stack is removed
   -- Exception: empty_stack - if the stack is empty
   function pop (s : in out stack) return integer;
   
   -- Role: Displays the stack as follow:
   -- | |
   -- |5|
   -- |4|
   -- ---
   -- Parameter: s - the stack to display
   -- Pre:
   -- Post:
   -- Exception: empty_stack - if the stack is empty
   procedure display_stack (s : in stack);
   

end pile;
