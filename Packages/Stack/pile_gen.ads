
generic
   type g_value is private;
   with function image (e : g_value) return string;

package pile_gen is

   type element is private;
   type stack is access element;
   
   empty_stack : exception;
   
   -- Role: Creates an empty stack
   -- Parameter: s - the stack to initialise
   -- Pre:
   -- Post: s is empty
   procedure create_stack (s : out stack);
   
   -- Role: Checks if the stack is empty
   -- Parameter: s - the stack to check
   -- Returns: TRUE if the stack is empty and FALSE otherwise
   -- Pre:
   -- Post:
   function is_empty (s : in stack) return boolean;
   
   -- Role: Pushes an element at the top of the stack
   -- Parameters: s - the stack to push the element into
   -- e - the element to push
   -- Pre:
   -- Post: the element is at the top of the stack
   procedure push (s : in out stack;
                   e : in g_value);
   
   -- Role: Pops the element at the top of the stack
   -- Parameter: s - the stack to pop the element from
   -- Returns: the first element of the stack
   -- Pre:
   -- Post: the first element of the stack is removed
   -- Exception: empty_stack - if the stack is empty
   function pop (s : in out stack) return g_value;

   -- Role: Peeks at the top element of the stack without poping it
   -- Parameter: s - the stack to peek the element on
   -- Returns: the first element of the stack
   -- Pre:
   -- Post:
   -- Exception: empty_stack - if the stack is empty
   function peek (s : in stack) return g_value;
   
   -- Role: Displays the stack as follow:
   -- |   |
   -- |  X|
   -- |  X|
   -- -----
   -- Parameter: s - the stack to display
   -- Pre:
   -- Post:
   -- Exception: empty_stack - if the stack is empty
   procedure display_stack (s : in stack);
   
   private
      type element is record
         value : g_value;
         next : stack;
      end record;

end pile_gen;
