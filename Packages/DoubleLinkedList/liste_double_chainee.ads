

package liste_double_chainee is
    type list_element;
    type list is access list_element;
    type list_element is record
        previous : list;
        next : list;
        value : integer;
    end record;

    no_such_element : exception;

    -- Role: Creates an empty list
    -- Parameter: l - list that will be initialised
    -- Pre:
    -- Post: the list is empty
    procedure create_list (l : out list);

    -- Role: Adds an element in the list
    -- Parameters: l - list to add the element in
    -- e - element to add in the list
    -- Pre:
    -- Post: the current element of the list is the element added and the list is ordered
    procedure add_element (l : in out list;
                           e : in integer);
    
    -- Role: Removes the specified element from the list
    -- Parameters: l - list to remove the element from
    -- e - element to remove
    -- Pre:
    -- Post: the element is removed from the list and the list is ordered
    -- Exception: no_such_element - if the specified element is not in the list
    procedure remove_element (l : in out list;
                              e : in integer);

    -- Role: Searches the specified element in the list
    -- Parameters: l - list to search the element in
    -- e - element to search
    -- Pre: 
    -- Post: the current element of the list is the element found
    -- Exception: no_such_element - if the specified element is not in the list
    procedure search_element (l : in out list;
                              e : in integer);
    
    -- Role: Displays all of the elements of the list, from the first to the last
    -- Parameter: l - list to display the element of
    -- Pre:
    -- Post:
    procedure display_list (l : in list);

end liste_double_chainee;