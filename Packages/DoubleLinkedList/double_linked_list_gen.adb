with ada.integer_text_io, ada.text_io;
use ada.integer_text_io, ada.text_io;

package body double_linked_list_gen is
    
    procedure create_list (l : out double_linked_list) is
    begin
        l := null;
    end create_list;

    function is_empty (l : in double_linked_list) return boolean is
    begin
        return l = null;
    end is_empty;

    procedure add_element (l : in out double_linked_list;
                           e : in g_value) is
        current : double_linked_list;
    begin
        if l = null then
            l := new list_element'(previous => null,
                                  next => null,
                                  value => e);
        else
            current := l;
            -- find last element
            while current.next /= null loop
                current := current.next;
            end loop;
            current.next := new list_element'(previous => l,
                                       next => null,
                                       value => e);
        end if;
    end add_element;

    procedure remove_element (l : in out double_linked_list;
                              e : in g_value) is
        parent : double_linked_list;
        current : double_linked_list;
    begin
        if l = null then
            null;
        else
            -- first element
            if l.value = e then
                l := l.next;
                if l.next /= null then
                    l.next.previous := null;
                else
                    null;
                end if;
            else
                null;
            end if;

            -- other elements
            parent := l;
            current := l.next;
            while current /= null loop
                if current.value = e then
                    -- remove
                    parent.next := current.next;
                    if current.next /= null then
                        current.next.previous := parent;
                    else
                        null;
                    end if;
                    current := null;
                else
                    parent := current;
                    current := current.next;
                end if;
            end loop;
        end if;
    end remove_element;

    function contains (l : in out double_linked_list;
                              e : in g_value) return boolean is
        current : double_linked_list;
        found : boolean;
    begin
        found := false;
        current := l;
        while current /= null and not found loop
            if current.value = e then
                found := true;
            else
                current := current.next;
            end if;
        end loop;
        
        return found;
    end contains;
    
    procedure display_list (l : in double_linked_list) is
        current_element : double_linked_list;
    begin
        if is_empty(l) then
            put_line("-empty-");
        else
            current_element := l;
            while current_element /= null loop
                put(image(current_element.value));
                if current_element.next /= null then
                    put(" ");
                else
                    null;
                end if;
                current_element := current_element.next;
            end loop;
            new_line;
        end if;
    end display_list;

end double_linked_list_gen;