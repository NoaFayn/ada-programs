
with ada.integer_text_io, ada.text_io;
use ada.integer_text_io, ada.text_io;

with double_linked_list_gen;

procedure test_double_linked_list_gen is
    package double_linked_list_integer is new double_linked_list_gen (integer, "=", integer'image);
    use double_linked_list_integer;

    assert_error : exception;

    procedure check (b : in boolean; msg : in string) is
    begin
        if b then
            put(".");
        else
            put("ERROR> ");
            put_line(msg);
            raise assert_error;
        end if;
    end check;

    l : double_linked_list;

begin

    create_list(l);
    check(is_empty(l), "List not empty");

    add_element(l, 1);
    check(contains(l, 1), "");

    add_element(l, 2);
    check(contains(l, 1), "");
    check(contains(l, 2), "");

    add_element(l, 3);
    check(contains(l, 1), "");
    check(contains(l, 2), "");
    check(contains(l, 3), "");

    add_element(l, 4);
    check(contains(l, 1), "");
    check(contains(l, 2), "");
    check(contains(l, 3), "");
    check(contains(l, 4), "");

    remove_element(l, 1);
    check(not contains(l, 1), "");
    check(contains(l, 2), "");
    check(contains(l, 3), "");
    check(contains(l, 4), "");

    remove_element(l, 2);
    check(not contains(l, 1), "");
    check(not contains(l, 2), "");
    check(contains(l, 3), "");
    check(contains(l, 4), "");

    remove_element(l, 4);
    check(not contains(l, 1), "");
    check(not contains(l, 2), "");
    check(contains(l, 3), "");
    check(not contains(l, 4), "");

    new_line;
    display_list(l);

end test_double_linked_list_gen;