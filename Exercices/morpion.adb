
with ada.Text_IO, ada.Integer_Text_IO;
use ada.Text_IO, ada.Integer_Text_IO;

procedure morpion is
   KMAX : constant integer := 10; -- max size of the board
   type symbol is (free,circle,cross); -- content of a cell of the board
   type board is array (1..KMAX, 1..KMAX) of symbol; -- board
   type player is (pcircle, pcross); -- two players
   type game_state is (playing, won, draw);

   -- Role: Switches to the next player
   -- Parameter: p - player to switch
   -- Pre:
   -- Post: p is the next player
   procedure next_player(p : in out player) is
   begin
      if p = pcircle then
         p := pcross;
      elsif p = pcross then
         p := pcircle;
      end if;
   end next_player;


   -- Role: Gets the symbol of the player
   -- Parameter: p - player to get the symbol of
   -- Returns: symbol associated with the player
   -- Pre: player is one of (pcircle,pcross)
   -- Post:
   function symbol_of_player(p : in player) return symbol is
      s : symbol; -- symbol of the player
   begin
      s := free;
      if p = pcircle then
         s := circle;
      elsif p = pcross then
         s := cross;
      end if;
      return s;
   end symbol_of_player;


   -- Role: Displays the player
   -- Parameter: p - player to display
   -- Pre:
   -- Post:
   procedure display_player(p : in player) is
   begin
      if p = pcircle then
         put("CIRCLE");
      else
         put("CROSS");
      end if;
   end display_player;


   -- Definition of functions and procedures
   -- Role: Initialises the game to be ready to start
   -- Parameters: damier - board
   -- n - dimensions of the board
   -- p - player to start
   -- Pre:
   -- Post: board is empty, n >= 3 and player = pcross
   procedure initialise_game(damier : out board ; n : out integer ; p : out player) is
   begin
      -- Get dimension of the board
      put("Enter size of the board (>=3 and <= ");put(KMAX,0);put(": ");
      begin
         n := integer'value(get_line);
      exception
         when Constraint_Error => n := 0;
      end;
      while n < 3 or n > KMAX loop
         put("Size must be >=3 and <= ");put(KMAX,0);put("! Enter size: ");
      begin
         n := integer'value(get_line);
      exception
         when Constraint_Error => n := 0;
      end;
      end loop;

      -- Fill board with empty cells
      for i in 1..n loop
         for j in 1..n loop
            damier(i,j) := free;
         end loop;
      end loop;

      -- First player
      p := pcross;
   end initialise_game;

   -- Role: Displays the board
   -- Parameters: damier - board to display
   -- n - dimension of the board
   -- Pre: n > 0
   -- Post:
   procedure display_board(damier : in board ; n : in integer) is
   begin
      for i in 1..n loop
         put("|");
         for j in 1..n loop
            if damier(i,j) = free then
               put(" ");
            elsif damier(i,j) = cross then
               put("X");
            elsif damier(i,j) = circle then
               put("O");
            end if;
            put("|");
         end loop;
         put_line("");
      end loop;

   end display_board;


   -- Role: Displays the board and the current player
   -- Parameters: damier - board
   -- n : dimension of the board
   -- p : current player
   -- Pre: damier is initialised
   -- Post:
   procedure display_game(damier : in board ; n : in integer ; p : in player) is
   begin
      -- Display current player
      put("Current player >> ");
      display_player(p);
      put_line("");

      -- Display board
      display_board(damier, n);

   end display_game;

   -- Role: Displays the board and the winner if there is any
   -- Parameters: damier - board to display
   -- n - dimension of the board
   -- p - player who won (if any, otherwise unused)
   -- state - state of the game at the end
   -- Pre: damier is in end situation, n > 0, state /= playing
   -- Post:
   procedure display_end_game(damier : in board ; n : in integer ; p : in player ; state : in game_state) is
   begin
      -- Display board
      display_board(damier, n);

      -- Display winner
      if state = won then
         put("Winner is ");
         display_player(p);
      else
         put("Draw!");
      end if;

   end display_end_game;

   -- Role: Asks to the player the line
   -- Parameter: n - dimension of the board
   -- Returns: line index
   -- Pre:
   -- Post: line is correct (>0 and <= n)
   function ask_line(n : in integer) return integer is
      line : integer;
   begin
      put("Enter the line: ");
      begin
         line := integer'value(get_line);
      exception
         when Constraint_Error => line := 0;
      end;
      while line <= 0 or line > n loop
         put("Wrong line. Please enter correct line: ");
      begin
         line := integer'value(get_line);
      exception
         when Constraint_Error => line := 0;
      end;
      end loop;
      return line;
   end ask_line;

   -- Role: Asks to the player the column
   -- Parameter: n - dimension of the board
   -- Returns: column index
   -- Pre:
   -- Post: column is correct (>0 and <= n)
   function ask_column(n : in integer) return integer is
      col : integer;
   begin
      put("Enter the column: ");
      begin
         col := integer'value(get_line);
      exception
            when Constraint_Error => col := -1;
      end;
      while col <= 0 or col > n loop
         put("Wrong column. Please enter correct col: ");
      begin
         col := integer'value(get_line);
      exception
            when Constraint_Error => col := -1;
      end;
      end loop;
      return col;
   end ask_column;


   -- Role: Play the turn for the specified player
   -- Parameters: damier - board
   -- n - dimension of the board
   -- p - current player to play
   -- state - state of the game
   -- Pre: damier is initialised, state = playing
   -- Post: damier is updated with the move of the player, p is the next player
   -- to play if the state is playing or the winner if the state is won or
   -- undefined if the state is draw, state is updated to reflect the new state
   -- of the game
   procedure play(damier : in out board ; n : in integer ; p : in out player ; state : in out game_state) is
      line : integer; -- line chosen by the player
      col : integer; -- column chosen by the player

      winColomn : boolean; -- true if a column is winning
      winLine : boolean; -- true if a line is winning
      winDiagonal : boolean; -- true if a diagonal is winning
      board_full : boolean; -- true if the board is full
   begin
      -- Ask player the position
      line := ask_line(n);
      col := ask_column(n);

      -- Check position is free
      while damier(line, col) /= free loop
         put_line("Position is not free! Please enter valid position:");
         line := ask_line(n);
         col := ask_column(n);
      end loop;

      -- Update position with new value
      if p = pcircle then
         damier(line,col) := circle;
      elsif p = pcross then
         damier(line,col) := cross;
      end if;

      -- Check winner or draw
      board_full := True;
      -- Check columns
      winColomn := True;
      for line in 1..n loop
         winColomn := True;
         for col in 1..n loop
            if damier(line,col) /= symbol_of_player(p) then
               winColomn := False;
            end if;

            -- Check board full
            if damier(line,col) = free then
               board_full := False;
            end if;
         end loop;
         exit when winColomn;
      end loop;

      if not winColomn then
         -- Check lines
         winLine := True;
         for col in 1..n loop
            winLine := True;
            for line in 1..n loop
               if damier(line,col) /= symbol_of_player(p) then
                  winLine := False;
               end if;
            end loop;
            exit when winLine;
         end loop;
      end if;

      if not winColomn and not winLine then
         -- Check diagonals
         -- Diagonal 1
         winDiagonal := True;
         for i in 1..n loop
            if damier(i,i) /= symbol_of_player(p) then
               winDiagonal := False;
            end if;
            exit when not winDiagonal;
         end loop;

         if not winDiagonal then
            winDiagonal := True;
            -- Diagonal 2
            for i in 1..n loop
               if damier(i,n-(i-1)) /= symbol_of_player(p) then
                  winDiagonal := False;
               end if;
               exit when not winDiagonal;
            end loop;
         end if;
      end if;


      -- Winner
      if winLine or winColomn or winDiagonal then
         state := won;
      elsif board_full then
         state := draw;
      else
         next_player(p);
      end if;

   end play;





   damier : board; -- board
   n : integer; -- dimensions of the board (between 3 and KMAX)
   current_player : player; -- current player
   state : game_state; -- current state of the game
begin

   -- Initialise the game
   initialise_game(damier, n, current_player);
   state := playing;

   -- Play
   loop
      -- Display game
      display_game(damier, n, current_player);

      -- Current player is playing
      play(damier, n, current_player, state);

      exit when state /= playing;
   end loop;

   -- Display end game
   display_end_game(damier, n, current_player, state);


end morpion;
