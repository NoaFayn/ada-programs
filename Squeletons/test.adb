
with ada.integer_text_io, ada.text_io;
use ada.integer_text_io, ada.text_io;

procedure test is

    procedure print_test_result (nb_tests_ok : in integer;
                                 nb_tests_error : in integer) is
    begin
        put(nb_tests_ok, 0);put("/");put(nb_tests_error+nb_tests_ok, 0);put(" tests ok");
        if nb_tests_error > 0 then
            put(", ");put(nb_tests_error, 0);put(" errors");
        end if;
        new_line;
    end print_test_result;

    procedure test_ is
        nb_tests_ok : integer;
        nb_tests_error : integer;
    begin
        put_line("test ");
        nb_tests_ok := 0;
        nb_tests_error := 0;

        if test_ok then
            nb_tests_ok := nb_tests_ok + 1;
        else
            nb_tests_error := nb_tests_error + 1;
            put_line("Error: msg");
            put("Got: ");
            new_line;
            put("Expected: ");
            new_line;
        end if;

        print_test_result(nb_tests_ok, nb_tests_error);
    end test_;

    -- Variables

begin

    null;
    test_;

end test;